if (window.JOS && typeof window.JOS.registerVersion == "function"){
  window.JOS.registerVersion(window.JOS.self)("strings")(window.hyperpay_strings_version)();
}
  window.getStrings = function(language){
    var strings  = {"english":{"upi_apps_header":"Send a Payment Request to your UPI app","upi_collect_req":"A payment request will be sent to this UPI ID","collectText":"Please open your UPI app and approve the payment request sent.","in_app_collect_header":"Approve Payment Request","netbanking_header":"NetBanking","netbanking":"NetBanking","netbanking_navbar":"NetBanking","proceed":"Proceed to Pay","pay_now":"Proceed to Pay","proceed_to_pay":"Proceed to Pay","placeOrder":"Proceed to Pay","processing_payment":"Processing...","card_navbar":"Credit / Debit Card","get_upi_textbox_heading":"UPI ID","other_upi_options":"Add upi id","upi":"Add UPI ID"}}
    return JSON.stringify(strings[language] || {});
  };