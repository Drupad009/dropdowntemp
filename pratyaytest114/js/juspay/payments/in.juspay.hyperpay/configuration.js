window.hyperpay_configuration.js_version = 2.0.0;
let fontName = "Cabin";
    let fonts = [
  {
    "name": "Cabin",
    "url": "https://assets.juspay.in/hyper/fonts/Cabin-Regular.ttf",
    "weight": 400
  },
  {
    "name": "Cabin",
    "url": "https://assets.juspay.in/hyper/fonts/Cabin-SemiBold.ttf",
    "weight": 500
  },
  {
    "name": "Cabin",
    "url": "https://assets.juspay.in/hyper/fonts/Cabin-Bold.ttf",
    "weight": 700
  }
];

    if(window.JBridge && typeof window.JBridge.loadFonts === 'function') {
    window.JBridge.loadFonts(fonts);
    }

    window.getMerchantConfig = function () {
      var configuration = {
  "componentMapping": {
    "*.Global": "globalConfig",
    "*.FlowConfig": "flowConfig",
    "*.ScreenConfig": "screenConfig",
    "*.EMIInstrumentsScreen.ListItem": "emiInstrumentListItem",
    "*.EMIStoredCard.ListItem": "emiStoredCardListItem",
    "*.EMIPlansScreen.ScreenConfig": "emiPlansScreenConfig",
    "*.EMICheckoutScreen.ScreenConfig": "emiCheckoutScreenConfig",
    "*.EMIOptionsScreen.ScreenConfig": "emiOptionsScreenConfig",
    "*.EMIAmountWidgetScreen.ScreenConfig": "emiPlansScreenConfig",
    "*.Toolbar": "defaultToolbar",
    "*.AmountBar": "defaultAmountBar",
    "*.WebWrapper.AmountBar": "defaultWebAmountBar",
    "*.WebWrapper.PaymentHeader.Toolbar": "defaultWebPaymentHeaderToolbar",
    "*.PrimaryButton": "defaultPrimaryButton",
    "*.Message": "defaultMessage",
    "*.EditText": "defaultEditText",
    "*.ListItem": "defaultListItem",
    "*.GridItem": "defaultGridItem",
    "*.SearchBox": "defaultSearchBox",
    "*.NavBar": "defaultNavBar",
    "*.AddCard": "defaultAddCard",
    "*.EMICheckoutScreen.AddCard": "emiCheckoutAddCard",
    "*.Popup": "defaultPopup",
    "*.SecondaryButton": "defaultSecondaryButton",
    "*.PrestoList": "defaultPrestoList",
    "*.Loader": "defaultLoaderConfig",
    "*.SavedCard.ListItem": "savedCardListItem",
    "*.EMIPlansScreen.ListItem": "emiPlansListItem",
    "*.EMIOptionsScreen.ListItem": "emiOptionsListItem",
    "*.EMICheckoutScreenInstrument.ListItem": "emiCheckoutListItemInstrument",
    "*.PaymentOption.ListItem": "paymentOptionListItem",
    "*.PaymentOption.GenericIntent.ListItem": "paymentOptionGenericIntentListItem",
    "*.AddButton.Toolbar": "webAddButtonToolbar",
    "*.Error.Message": "errorMessage",
    "*.ExpandedViews.ListItem": "expandedViewsListItem",
    "*.OtherBanks.ListItem": "otherBanksListItem",
    "*.SavedVPA.ListItem": "ppSavedVPAListItem",
    "*.InApp.ListItem": "inAppListItem",
    "*.Rewards.ListItem": "rewardsListItem",
    "*.WalletVerifyNumberScreen.EditText": "verifyNumberEditText",
    "*.WalletVerifyOTPScreen.EditText": "verifyOtpEditText",
    "*.PaymentOption.FoodCards.ListItem": "paymentOptionFoodCardsListItem",
    "*.PaymentInfo.Message": "paymentInfoMessage",
    "*.PaymentBottomInfo.Message": "paymentBottomInfo",
    "*.Surcharge.Message": "surchargeMessage",
    "*.Outage.Message": "defaultMessage",
    "RewardsPopup.ScreenConfig": "rewardsScreenConfig",
    "EnableSI.Message": "enableSIBar",
    "RewardsPay.Message": "rewardsPayMessage",
    "SaveDefault.Message": "defaultOptionBar",
    "MandateEducation.PrimaryButton": "mandateEducationPrimaryButton",
    "RewardsEducation.PrimaryButton": "rewardsEducationPrimaryButton",
    "*.OtherUPI.SecondaryButton": "otherUPISecondaryButton",
    "NBScreen.PrimaryButton": "nbPrimaryButton",
    "SingleCard.Message": "singleCardMessage",
    "Screen.Popup": "deletePopupConfig",
    "PaymentManagementScreen.Popup": "deletePopupConfig",
    "PaymentPageScreen.PaymentOption.PaymentManagement.ListItem": "pmListItem",
    "PaymentStatus.Popup": "paymentStatusPopup",
    "RetrySuggestion.ListItem": "retrySuggestionListItem",
    "ViesEnrollment.Popup": "viesEnrollmentPopupConfig",
    "BackPressDialog.Popup": "backPressDialogPopup",
    "WalletScreen.UnLinked.ListItem": "unlinkedWalletListItem",
    "WalletScreen.Linked.ListItem": "linkedWalletListItem",
    "PaymentManagement.SavedCard.ListItem": "pmSavedCardListItem",
    "PaymentManagement.SavedVPA.ListItem": "pmSavedVPAListItem",
    "QuickPayScreen.Linked.ListItem": "quickPayLinkedWallet",
    "QuickPayScreen.SavedCard.ListItem": "quickPaySavedCard",
    "QuickPayScreen.NetBank.ListItem": "quickPayNB",
    "QuickPayScreen.UpiCollect.ListItem": "quickPayUpiCollect",
    "QuickPayScreen.UnlinkedWallets.ListItem": "quickPayUnlinkedWallet",
    "QuickPayScreen.PrimaryButton": "quickPayPrimaryButton",
    "NBScreen.MandateConsent.Message": "nbMandateConsentMessage",
    "PaymentPage.ExpandedNB.ListItem": "expandedNBBottomListItem",
    "PaymentStatusScreen.ListItem": "paymentStatusListItem",
    "PaymentStatus.AmountBar": "paymentStatusAmountBar",
    "PaymentStatusScreen.SecondaryButton": "paymentStatusSecondaryButton",
    "PaymentStatusScreen.PrimaryButton": "paymentStatusPrimaryButton",
    "NBScreen.SearchBox": "nbScreenSearchBox",
    "*.WalletScreen.ScreenConfig": "walletScreenConfig",
    "PaymentPage.Expanded.LinkedWallet.ListItem": "linkedWalletListItem",
    "PaymentPage.Expanded.UnlinkedWallet.ListItem": "unlinkedWalletListItem",
    "UPIScreen.SavedVPA.ListItem": "savedVPAListItem",
    "PaymentPageScreen.AmountBar": "ppAmountBar",
    "PaymentPageScreen.Toolbar": "ppToolbar",
    "NBScreen.OtherBanks.SecondaryButton": "nbOtherBanksSecondaryButton",
    "UPIScreen.OtherUPI.SecondaryButton": "upiOtherOptionsSecondaryButton",
    "PaymentPage.SavedCard.ListItem": "ppSavedCardListItem",
    "NBScreen.OtherBanks.ListItem": "nbScreenOtherBanksListItem",
    "UPIScreen.UPIApp.ListItem": "upiAppListItem",
    "WebWrapper.PaymentHeader.Toolbar": "webPaymentHeaderToolbar",
    "WebWrapper.Back.Toolbar": "webBackToolBar",
    "COD.ScreenConfig": "codScreen",
    "PaymentPageScreen.CashOD.ListItem": "unlinkedWalletListItem",
    "UPIScreen.*.EditText": "upiScreenEditText",
    "UPIAddScreen.ScreenConfig": "upiAddScreen"
  },
  "mainConfig": {
    "globalConfig": {
      "primaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.primaryColor"
        ]
      },
      "secondaryColor": "#D6D6D6",
      "textPrimaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.defaultTextColor"
        ]
      },
      "textSecondaryColor": "#999999",
      "textTertiaryColor": "#999999",
      "errorColor": {
        "#ref": [
          "masterConfig.themes.Colors.errorColor"
        ]
      },
      "successColor": {
        "#ref": [
          "masterConfig.themes.Colors.successColor"
        ]
      },
      "dividerColor": "#e9e9e9",
      "hintColor": "#999999",
      "checkboxFontColor": "#6B6B6B",
      "primaryFont": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Regular\";"
          ]
        }
      },
      "checkboxFont": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Regular\";"
          ]
        }
      },
      "fontBold": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Bold\";"
          ]
        }
      },
      "fontSemiBold": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-SemiBold\";"
          ]
        }
      },
      "fontRegular": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Regular\";"
          ]
        }
      },
      "checkboxSize": 16,
      "fontSize": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.TypoGraphy.desktopFontBaseSize"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.TypoGraphy.fontBaseSize"
            ]
          }
        ]
      }
    },
    "flowConfig": {
      "showSavedVPAs": false,
      "upiQREnable": true,
      "popularBanks": [
        "NB_SBI",
        "NB_HDFC",
        "NB_ICICI",
        "NB_AXIS"
      ],
      "paymentOptions": {
        "#js-expr": [
          "var po_length = rc('masterConfig.screen.sideBarTabsRef.orderOfElements').length; var arr_obj = []; for(var i=0;i<po_length;i++) { var pos = rc('masterConfig.screen.sideBarTabsRef.orderOfElements')[i]; arr_obj.push({group: 'others',po: pos,onlyDisable: [],visibility: \"VISIBLE\"}); } arr_obj;"
        ]
      },
      "sideBarTabs": {
        "#if": [
          "window.__payload.action !== 'paymentManagement'",
          {
            "#ref": [
              "flowConfig.sideBarTabsRef"
            ]
          },
          {
            "#js-expr": [
              "rc('flowConfig.sideBarTabsRef').includes('MANAGE') ? ['MANAGE']:[]"
            ]
          }
        ]
      },
      "firstLoadSideBarTab": {
        "#js-expr": [
          "rc('flowConfig.sideBarTabsRef')[0]"
        ]
      },
      "mandateInstruments": {
        "#js-expr": [
          "var mi = [];  if (rc('masterConfig.screen.sideBarTabsRef.dataOfElements.cards.mandates')) mi.push(\"cards\"); if (rc('masterConfig.screen.sideBarTabsRef.dataOfElements.upi.mandates')) \n  {mi.push(\"intent\");mi.push(\"collect\")}\nif (rc('masterConfig.screen.sideBarTabsRef.dataOfElements.wallets.mandates')) mi.push(\"cards\"); mi;"
        ]
      },
      "outageViewProps": {
        "showOutageView": {
          "#if": [
            {
              "#ref": [
                "masterConfig.screen.features.outage"
              ]
            },
            "true",
            "false"
          ]
        }
      },
      "flows": {
        "directOTP": {
          "#if": [
            {
              "#ref": [
                "masterConfig.screen.features.nativeOTP"
              ]
            },
            "true",
            "false"
          ]
        },
        "hideIneligiblePayLater": {
          "#ref": [
            "masterConfig.screen.sideBarTabsRef.dataOfElements.paylater.hideIneligiblePayLater"
          ]
        },
        "useSilentMandateEnforcement": {
          "#if": [
            {
              "#ref": [
                "masterConfig.screen.features.silentlyEnforceMandate"
              ]
            },
            "true",
            "false"
          ]
        },
        "enforceSaveCard": {
          "#ref": [
            "masterConfig.screen.sideBarTabsRef.dataOfElements.cards.mandatorySaveCard"
          ]
        },
        "mandateUPIHandles": {
          "#if": [
            {
              "#ref": [
                "masterConfig.screen.sideBarTabsRef.dataOfElements.upi.mandates"
              ]
            },
            [
              "ybl",
              "axl",
              "paytm",
              "upi"
            ],
            []
          ]
        }
      },
      "payeeName": "Juspay",
      "drawFromStatusBar": false,
      "upiConfig": {
        "skipHomeScreen": true
      },
      "verifyVpa": {
        "#ref": [
          "masterConfig.screen.sideBarTabsRef.dataOfElements.upi.verifyVpa"
        ]
      },
      "offers": {
        "isEnabled": {
          "#ref": [
            "masterConfig.components.offers.visible"
          ]
        },
        "isInstantDiscount": true
      },
      "sideBarTabsRef": [
        "CARD",
        "WALLET",
        "UPI",
        "NET_BANKING",
        "INAPPS",
        "PAY_LATER",
        "COD",
        "EMI"
      ]
    },
    "upiAddScreen": {
      "#override": [
        "screenConfig",
        {
          "uiCard.horizontalPadding": 20,
          "uiCard.verticalPadding": 20
        }
      ]
    },
    "codScreen": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.text": "Pay On Delivery"
        }
      ]
    },
    "walletScreenConfig": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                26,
                26,
                0,
                10
              ],
              {
                "#ref": [
                  "masterConfig.components.container.sectionHeaderPadding"
                ]
              }
            ]
          },
          "containerAttribs.horizontalSpacing": {
            "#js-expr": [
              " var padding = rc('masterConfig.components.container.containerPadding');\n     padding[0];"
            ]
          }
        }
      ]
    },
    "emiOptionsScreenConfig": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.text": "EMI Options",
          "utils.contentMargin": {
            "#js-expr": [
              " var vSpace = rc('screenConfig.containerAttribs.verticalSpacing');\n            var hSpace = rc('screenConfig.containerAttribs.horizontalSpacing') + 12;\n            if(rc('screenConfig.containerAttribs.horizontalSpacing') == 0){\n              window.isDesktopView() ? [2,0,0,0] : rc('masterConfig.components.container.containerPadding')\n            }\n            else{\n              window.isDesktopView() ? [2,0,0,0] : [100, vSpace, hSpace, vSpace]\n            }\n          "
            ]
          }
        }
      ]
    },
    "screenConfig": {
      "bgPrimaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.backgroundColor"
        ]
      },
      "bgSecondaryColor": "#FDFDFD",
      "containerAttribs": {
        "horizontalSpacing": 0,
        "verticalSpacing": 0,
        "sectionSpacing": {
          "#ref": [
            "masterConfig.components.container.cardMargin"
          ]
        },
        "margin": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              24,
              10,
              0,
              24
            ],
            {
              "#ref": [
                "masterConfig.components.container.containerPadding"
              ]
            }
          ]
        }
      },
      "utils": {
        "contentMargin": [
          0,
          0,
          0,
          0
        ],
        "sectionMargin": [
          0,
          0,
          0,
          {
            "#ref": [
              "masterConfig.components.container.cardMargin"
            ]
          }
        ]
      },
      "uiCard": {
        "translation": {
          "#if": [
            {
              "#ref": [
                "masterConfig.themes.Shadow.cardShadow.visible"
              ]
            },
            {
              "#js-expr": [
                "var nativeShadow = rc('masterConfig.themes.Shadow.cardShadow.nativeShadow');\nnativeShadow == 0 ? 0.1 : nativeShadow"
              ]
            },
            0.1
          ]
        },
        "cornerRadius": {
          "#ref": [
            "masterConfig.components.container.cornerRadius"
          ]
        },
        "horizontalPadding": {
          "#js-expr": [
            "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 0 : padding"
          ]
        },
        "verticalPadding": 10,
        "color": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            "#ffffff",
            {
              "#ref": [
                "masterConfig.themes.Colors.defaultTileColor"
              ]
            }
          ]
        },
        "stroke": {
          "#js-expr": [
            "var strokeColor = rc('masterConfig.components.container.strokeColor');\nvar strokeWidth = rc('masterConfig.components.container.strokeWidth');\n                  window.isDesktopView() ? '' : strokeWidth+\",\"+strokeColor"
          ]
        },
        "addStrokeToForm": true,
        "shadow": {
          "spread": {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.spread"
            ]
          },
          "blur": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.blur"
                ]
              },
              0
            ]
          },
          "opacity": 0,
          "hOffset": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.xOffset"
                ]
              },
              0
            ]
          },
          "vOffset": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.yOffset"
                ]
              },
              0
            ]
          },
          "color": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.color"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Colors.backgroundColor"
                ]
              }
            ]
          }
        }
      },
      "button": {
        "background": {
          "#ref": [
            "screenConfig.bgPrimaryColor"
          ]
        },
        "maxWidth": {
          "#js-expr": [
            "(window.__OS.toLowerCase() === \"web\") && window.isDesktopView() ? 223 : \"match_parent\""
          ]
        }
      },
      "sectionHeader": {
        "font": {
          "#js-expr": [
            "if (window.isDesktopView()) {\n        rc('globalConfig.fontSemiBold')\n      } else {\n        rc('globalConfig.fontBold')\n      }"
          ]
        },
        "textSize": {
          "#ref": [
            "masterConfig.themes.TypoGraphy.fontBaseSize"
          ]
        },
        "margin": {
          "#js-expr": [
            " if (window.isDesktopView()) {\n            [26, 24, 24, 12]\n          } else {\n            var uiPadding = rc('screenConfig.uiCard.horizontalPadding');\n            var hSpace = rc('screenConfig.containerAttribs.horizontalSpacing');\n            var bottomMargin = rc('screenConfig.sectionHeader.bottomMargin');\n            var tM = (rc('screenConfig.uiCard.translation') == 0.0) ? 0 : 4\n            if (hSpace == 0){\n              [0, 0, uiPadding, bottomMargin]\n            }\n            else {\n              [0, 0, 0, bottomMargin]\n            }\n          }\n        "
          ]
        },
        "padding": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              0,
              0,
              18,
              10
            ],
            {
              "#ref": [
                "masterConfig.components.container.sectionHeaderPadding"
              ]
            }
          ]
        },
        "dividerHeight": 1,
        "dividerColor": {
          "#ref": [
            "masterConfig.components.separator.color"
          ]
        },
        "dividerVisibility": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            "GONE",
            {
              "#if": [
                {
                  "#ref": [
                    "masterConfig.components.separator.visible"
                  ]
                },
                "VISIBLE",
                "GONE"
              ]
            }
          ]
        },
        "color": "#444444",
        "background": "transparent"
      },
      "sideBar": {
        "background": "#f8f8f8",
        "icon": {
          "selectedColor": {
            "#ref": [
              "masterConfig.desktopView.icon.selectedColor"
            ]
          },
          "notSelectedColor": {
            "#ref": [
              "masterConfig.desktopView.icon.notSelectedColor"
            ]
          }
        },
        "navbarItem": {
          "selectedBackgroundColor": {
            "#ref": [
              "masterConfig.desktopView.sideBar.selectedBackgroundColor"
            ]
          },
          "fontColor": {
            "#ref": [
              "masterConfig.desktopView.sideBar.unSelectedTextColor"
            ]
          },
          "selectedFontColor": {
            "#ref": [
              "masterConfig.desktopView.sideBar.selectedTextColor"
            ]
          }
        }
      },
      "expand": {
        "walletView": {
          "#ref": [
            "masterConfig.expand.walletView"
          ]
        },
        "popularNBView": true,
        "cod": true
      },
      "nb": {
        "useV2": true,
        "popularBanksBanksHeader": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            false,
            true
          ]
        },
        "gridViewPadding": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              15,
              8,
              8,
              8
            ],
            [
              8,
              8,
              8,
              8
            ]
          ]
        },
        "addMargin": false,
        "showPopular": true
      },
      "upi": {
        "showAddUpiHeader": true
      },
      "card": {
        "screenHeaderTextConfig": {
          "margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                28,
                10,
                0,
                10
              ],
              [
                0,
                0,
                0,
                0
              ]
            ]
          }
        }
      },
      "sideBar.navbarItem.selectedStroke": {
        "#js-expr": [
          "\"6,\" + rc('globalConfig.primaryColor') + \",l\""
        ]
      }
    },
    "masterConfig": {
      "themes": {
        "Colors": {
          "primaryColor": "#ff0099FF",
          "backgroundColor": "#ffffff",
          "errorColor": "#ffCC0000",
          "successColor": "#ff239b1b",
          "defaultTextColor": "#ff000000",
          "defaultTileColor": "#ffffffff"
        },
        "TypoGraphy": {
          "fontBaseSize": 14,
          "desktopFontBaseSize": 20,
          "fontFamily": "Cabin"
        },
        "Shadow": {
          "cardShadow": {
            "visible": true,
            "blur": 23,
            "yOffset": 6,
            "xOffset": 1,
            "spread": 0,
            "nativeShadow": 1,
            "color": "#fff1f1f1"
          }
        }
      },
      "components": {
        "buttons": {
          "strokeColor": "#000000",
          "strokeWidth": "0",
          "strokeVisibility": false,
          "cornerRadius": 5,
          "fontColor": "#ffffff",
          "fontSize": 14,
          "fontWeight": "Medium",
          "text": "Proceed to Pay",
          "margin": [
            0,
            16,
            0,
            0
          ]
        },
        "searchBox": {
          "leftImage": {
            "size": 16,
            "margin": [
              0,
              0,
              8,
              0
            ]
          },
          "nonActiveState": {
            "strokeColor": "#fff1f1f1"
          }
        },
        "inputFields": {
          "type": "box",
          "disabledStateColor": "#E9E9E9",
          "activeStateColor": {
            "#ref": [
              "masterConfig.themes.Colors.primaryColor"
            ]
          },
          "upiVerifyTextColor": "#ff0099FF",
          "useMaterialView": false,
          "fieldName": {
            "fontColor": "#E9E9E9",
            "fontSize": 10,
            "fontWeight": "bold",
            "padding": [
              5,
              0,
              5,
              0
            ]
          },
          "inputText": {
            "fontColor": "#333333",
            "fontSize": 14,
            "fontWeight": "bold",
            "padding": [
              0,
              0,
              10,
              0
            ]
          }
        },
        "container": {
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "strokeColor": "#ffe5e7eb",
          "strokeWidth": 1,
          "cornerRadius": 6,
          "sectionHeaderPadding": [
            3,
            10,
            0,
            10
          ],
          "containerPadding": [
            20,
            24,
            20,
            20
          ],
          "cardMargin": 24,
          "cardPadding": 16
        },
        "links": {
          "fontColor": {
            "#ref": [
              "masterConfig.themes.Colors.primaryColor"
            ]
          },
          "fontSize": 14,
          "fontWeight": "bold",
          "marginUpi": 10,
          "marginNb": 0
        },
        "offers": {
          "containerColor": "#ffccff",
          "containerRadius": 12,
          "visible": false,
          "otherIcons": {
            "iconColor": "#ffccff",
            "fontColor": "#ffccff",
            "fontSize": 12,
            "fontWeight": "bold",
            "padding": [
              1,
              0,
              0,
              1
            ]
          }
        },
        "separator": {
          "color": "#d0021b",
          "visible": false
        },
        "grid": {
          "fontColor": "#000000",
          "fontSize": 12,
          "fontWeight": "bold",
          "iconSize": 28
        },
        "listItems": {
          "paddings": [
            8,
            0,
            0,
            0
          ],
          "mainText": {
            "fontColor": {
              "#ref": [
                "masterConfig.themes.Colors.defaultTextColor"
              ]
            },
            "fontSize": {
              "#ref": [
                "masterConfig.themes.TypoGraphy.fontBaseSize"
              ]
            },
            "fontWeight": "bold"
          },
          "subText": {
            "fontColor": "#ff555555",
            "fontSize": 12,
            "fontWeight": "bold"
          },
          "spacingBetween": 4,
          "primaryIconSize": 32,
          "secondaryIconSize": 20
        },
        "appBar": {
          "fillColor": {
            "#ref": [
              "masterConfig.themes.Colors.primaryColor"
            ]
          },
          "padding": [
            8,
            0,
            0,
            0
          ],
          "fontColor": "#ffffffff",
          "fontSize": 16,
          "fontWeight": "bold"
        },
        "OrderSummary": {
          "visible": true,
          "layout": "Boxed (at top)",
          "containerStyle": 2,
          "containerColor": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 10,
          "labelText": {
            "fontColor": "#ff555555",
            "fontSize": 16
          },
          "orderNameText": {
            "fontColor": "#ff000000",
            "fontSize": 12
          },
          "amountText": {
            "fontColor": "#000000",
            "fontSize": 16
          },
          "spacingBetween": 4
        }
      },
      "desktopView": {
        "OrderSummary": {
          "visible": true,
          "backgroundColor": "#ffffff",
          "orderSummaryHeader": {
            "fontColor": "#363636"
          },
          "labelText": {
            "fontColor": "#999999",
            "fontSize": 12
          },
          "orderNameText": {
            "fontColor": "#363636"
          },
          "amountText": {
            "fontColor": "#363636"
          }
        },
        "icon": {
          "selectedColor": "#000000",
          "notSelectedColor": "#354052"
        },
        "sideBar": {
          "selectedBackgroundColor": "#DBDBDB",
          "selectedTextColor": "#121212",
          "unSelectedTextColor": "#444549"
        }
      },
      "expand": {
        "walletView": false
      },
      "screen": {
        "sideBarTabsRef": {
          "orderOfElements": [
            "foodcards",
            "nb",
            "emi",
            "upi",
            "cred",
            "upiApps",
            "googlepay",
            "cashod",
            "cards",
            "paylater"
          ],
          "dataOfElements": {
            "cards": {
              "mandates": true,
              "mandatorySaveCard": false
            },
            "foodcards": {},
            "upi": {
              "intent": true,
              "collect": true,
              "mandates": true,
              "verifyVpa": false
            },
            "nb": {
              "mandates": true
            },
            "cred": {},
            "paylater": {
              "hideIneligiblePayLater": true
            },
            "upiApps": {},
            "googlepay": {},
            "emi": {},
            "cashod": {},
            "wallets": {
              "mandates": true
            }
          }
        },
        "features": {
          "nativeOTP": true,
          "quickPay": true,
          "silentlyEnforceMandate": true,
          "outage": true,
          "guestUserFlow": true
        }
      }
    },
    "defaultLoaderConfig": {
      "dot": {
        "background": "#ffffff"
      },
      "text": {
        "color": "#FF0000"
      }
    },
    "defaultPrestoList": {
      "padding": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            0,
            0,
            24,
            0
          ],
          {
            "#js-expr": [
              "var hSpace = rc('masterConfig.components.container.cardPadding');\n[hSpace, 0, hSpace, 0]"
            ]
          }
        ]
      },
      "margin": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            25,
            0,
            28,
            0
          ],
          [
            0,
            0,
            0,
            0
          ]
        ]
      },
      "font": {
        "#ref": [
          "globalConfig.fontRegular"
        ]
      },
      "textSize": {
        "#ref": [
          "masterConfig.components.listItems.mainText.fontSize"
        ]
      },
      "leftImage": {
        "size": {
          "#ref": [
            "masterConfig.components.listItems.primaryIconSize"
          ]
        }
      },
      "rightImage": {
        "size": {
          "#ref": [
            "masterConfig.components.listItems.secondaryIconSize"
          ]
        }
      },
      "hasButton": true,
      "space": 16
    },
    "upiOtherOptionsSecondaryButton": {
      "#override": [
        "defaultSecondaryButton",
        {
          "text.text": "Other UPI Options",
          "margin": [
            {
              "#ref": [
                "masterConfig.components.links.marginUpi"
              ]
            },
            0,
            0,
            10
          ],
          "text.size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "text.color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 2,
          "stroke": "",
          "text.padding": [
            0,
            2,
            4,
            2
          ],
          "height": 30,
          "width": 130
        }
      ]
    },
    "nbOtherBanksSecondaryButton": {
      "#override": [
        "defaultSecondaryButton",
        {
          "text.text": "Other Banks",
          "margin": [
            {
              "#ref": [
                "masterConfig.components.links.marginNb"
              ]
            },
            0,
            0,
            10
          ],
          "text.size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "text.color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 2,
          "stroke": "",
          "text.padding": [
            0,
            2,
            4,
            2
          ],
          "height": 30,
          "width": 130
        }
      ]
    },
    "defaultSecondaryButton": {
      "#override": [
        "defaultPrimaryButton",
        {
          "cornerRadius": 15,
          "translation": 0,
          "margin": [
            0,
            0,
            0,
            0
          ],
          "stroke": "0,#ffffff",
          "color": {
            "#ref": [
              "screenConfig.uiCard.color"
            ]
          },
          "text.color": {
            "#ref": [
              "globalConfig.primaryColor"
            ]
          },
          "text.size": 14,
          "text.font": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          }
        }
      ]
    },
    "defaultAddCard": {
      "cardConfig": {
        "padding": {
          "#js-expr": [
            " var hP = rc('screenConfig.uiCard.horizontalPadding');\n        var vP = rc('screenConfig.uiCard.verticalPadding');\n        if (window.isDesktopView()) {\n          [11, 0, 0, 0]\n        } else {\n          [hP, vP, hP, vP + 12]\n        }\n      "
          ]
        },
        "cornerRadius": {
          "#js-expr": [
            " var hP = rc('screenConfig.containerAttribs.horizontalSpacing');\n        if (hP == 0) {\n          rc('masterConfig.components.container.cornerRadius')\n        } else {\n          rc('masterConfig.components.container.cornerRadius')\n        }\n      "
          ]
        }
      },
      "cardNumber": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.textSecondaryColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "cardNumberConfig",
            {
              "input.height": 48
            }
          ]
        },
        "inputFieldMargin": {
          "#if": [
            {
              "#js-expr": [
                "window.__OS == 'ANDROID'"
              ]
            },
            [
              0,
              0,
              0,
              10
            ],
            [
              0,
              8,
              0,
              20
            ]
          ]
        }
      },
      "expiry": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.textSecondaryColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "expiryDateConfig",
            {
              "icon.visibility": "gone",
              "input.height": 48
            }
          ]
        }
      },
      "cvv": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.hintColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "cvvConfig",
            {
              "input.height": 48
            }
          ]
        }
      },
      "saveCard": {
        "text": "Securely save this card for future payments.",
        "textSize": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            {
              "#ref": [
                "globalConfig.fontSizeSmall"
              ]
            },
            {
              "#ref": [
                "globalConfig.fontSizeVerySmall"
              ]
            }
          ]
        },
        "textFont": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        },
        "margin": [
          0,
          0,
          0,
          0
        ],
        "infoIcon": {
          "visible": false
        }
      },
      "si": {
        "textFont": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        }
      },
      "enforceCard": {
        "textFont": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        }
      },
      "warningMessage": {
        "textFont": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        }
      },
      "payButtonConfig": {
        "#override": [
          "defaultPrimaryButton",
          {
            "margin": {
              "#if": [
                {
                  "#js-expr": [
                    "window.isDesktopView()"
                  ]
                },
                [
                  28,
                  0,
                  0,
                  0
                ],
                {
                  "#js-expr": [
                    "var containerPadding = rc('masterConfig.components.container.containerPadding');\n                  var buttonPadding = rc('masterConfig.components.buttons.margin');\n                  var leftPadding = containerPadding[0] + buttonPadding[0];\n                  var topPadding = buttonPadding[1];\n                  var rightPadding = containerPadding[2] + buttonPadding[2];\n                  var bottomPadding = buttonPadding[3];\n                  [leftPadding, topPadding, rightPadding, bottomPadding];"
                  ]
                }
              ]
            },
            "width": {
              "#if": [
                {
                  "#js-expr": [
                    "window.isDesktopView()"
                  ]
                },
                250,
                "match_parent"
              ]
            }
          }
        ]
      },
      "cardConfig.margin": {
        "#js-expr": [
          "var vP = rc('screenConfig.containerAttribs.verticalSpacing');\n                  var tM = rc('screenConfig.utils.translationMargin');\n                  window.isDesktopView() ? [16, vP, tM, vP] : rc('masterConfig.components.container.containerPadding')"
        ]
      }
    },
    "defaultNavBar": {
      "background": "#F8F8F8",
      "textSize": {
        "#js-expr": [
          "rc('globalConfig.fontSize') - 1"
        ]
      },
      "selectedBackground": "#ffffff"
    },
    "nbScreenSearchBox": {
      "#override": [
        "defaultSearchBox",
        {
          "stroke": {
            "#js-expr": [
              "var strokeColor = rc('masterConfig.components.searchBox.nonActiveState.strokeColor');\nwindow.isDesktopView() ? \"1,\"+strokeColor : \"1,\"+strokeColor"
            ]
          }
        }
      ]
    },
    "defaultSearchBox": {
      "leftImage": {
        "size": {
          "#ref": [
            "masterConfig.components.searchBox.leftImage.size"
          ]
        },
        "margin": {
          "#ref": [
            "masterConfig.components.searchBox.leftImage.margin"
          ]
        }
      },
      "stroke": {
        "#ref": [
          "defaultEditText.stroke"
        ]
      },
      "height": 40,
      "padding": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            5,
            4,
            0,
            0
          ],
          [
            12,
            0,
            12,
            0
          ]
        ]
      },
      "margin": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            0,
            0,
            0,
            0
          ],
          [
            4,
            4,
            4,
            4
          ]
        ]
      },
      "cornerRadius": {
        "#ref": [
          "defaultEditText.cornerRadius"
        ]
      }
    },
    "upiScreenEditText": {
      "#override": [
        "defaultEditText",
        {
          "icon.textColor": {
            "#ref": [
              "masterConfig.components.inputFields.upiVerifyTextColor"
            ]
          },
          "header.color": {
            "#ref": [
              "masterConfig.components.inputFields.activeStateColor"
            ]
          }
        }
      ]
    },
    "verifyOtpEditText": {
      "#override": [
        "defaultEditText",
        {
          "icon.height": 25,
          "icon.textVisibility": "visible",
          "icon.visibility": "visible",
          "icon.text": "Resend OTP",
          "icon.textColor": "#634CBB",
          "header.text": "OTP",
          "input.inpType": "Numeric",
          "input.separator": "",
          "input.textMargin": [
            12,
            0,
            12,
            0
          ],
          "focus": true,
          "input.textSize": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              20,
              {
                "#ref": [
                  "globalConfig.fontSizeLarge"
                ]
              }
            ]
          },
          "margin": [
            10,
            10,
            0,
            12
          ],
          "header.color": {
            "#ref": [
              "masterConfig.components.inputFields.activeStateColor"
            ]
          }
        }
      ]
    },
    "verifyNumberEditText": {
      "#override": [
        "defaultEditText",
        {
          "input.pattern": "^[0-9]+$,10",
          "input.inpType": "Numeric",
          "input.separator": "",
          "input.textMargin": [
            12,
            0,
            12,
            0
          ],
          "focus": true,
          "input.textSize": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              20,
              {
                "#ref": [
                  "globalConfig.fontSizeLarge"
                ]
              }
            ]
          },
          "header.color": {
            "#ref": [
              "masterConfig.components.inputFields.activeStateColor"
            ]
          }
        }
      ]
    },
    "defaultEditText": {
      "margin": [
        10,
        10,
        0,
        10
      ],
      "useMaterialView": {
        "#ref": [
          "masterConfig.components.inputFields.useMaterialView"
        ]
      },
      "cornerRadius": {
        "#js-expr": [
          "var radius = rc('masterConfig.components.buttons.cornerRadius');\nvar height = rc('defaultPrimaryButton.height');\nvar val = height/2;\nif(val <= radius){\n  radius = val;\n}\n                  radius"
        ]
      },
      "stroke": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.inputFields.type"
                ]
              },
              "underline"
            ]
          },
          "",
          {
            "#js-expr": [
              "var color = rc('masterConfig.components.inputFields.disabledStateColor');\n          \"1,\" + color"
            ]
          }
        ]
      },
      "header": {
        "color": {
          "#ref": [
            "masterConfig.components.inputFields.fieldName.fontColor"
          ]
        },
        "size": {
          "#if": [
            {
              "#js-expr": [
                "window.isAndroid()"
              ]
            },
            {
              "#ref": [
                "masterConfig.components.inputFields.fieldName.fontSize"
              ]
            },
            {
              "#ref": [
                "masterConfig.components.inputFields.fieldName.fontSize"
              ]
            }
          ]
        },
        "padding": {
          "#ref": [
            "masterConfig.components.inputFields.fieldName.padding"
          ]
        }
      },
      "icon": {
        "width": {
          "#js-expr": [
            "rc('globalConfig.iconSize') - 20"
          ]
        },
        "height": {
          "#js-expr": [
            "rc('globalConfig.iconSize') - 20"
          ]
        },
        "textColor": {
          "#ref": [
            "globalConfig.primaryColor"
          ]
        }
      },
      "lineSeparator": {
        "color": {
          "#ref": [
            "masterConfig.components.inputFields.disabledStateColor"
          ]
        },
        "focusedColor": {
          "#ref": [
            "masterConfig.components.inputFields.activeStateColor"
          ]
        }
      },
      "input": {
        "padding": [
          20,
          0,
          20,
          0
        ],
        "height": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            48,
            48
          ]
        },
        "font": {
          "#ref": [
            "globalConfig.fontSemiBold"
          ]
        },
        "width": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            399,
            "match_parent"
          ]
        },
        "textColor": {
          "#ref": [
            "masterConfig.components.inputFields.inputText.fontColor"
          ]
        },
        "textSize": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            20,
            {
              "#ref": [
                "masterConfig.components.inputFields.inputText.fontSize"
              ]
            }
          ]
        }
      }
    },
    "nbPrimaryButton": {
      "#override": [
        "defaultPrimaryButton",
        {
          "margin": {
            "#js-expr": [
              "if (window.isDesktopView()) {\n        var cardPadding = rc('masterConfig.components.container.cardPadding');\n        var val = 10 + cardPadding;\n        [25, 0, 0,10]\n      } else {\n        var cardPadding = rc('masterConfig.components.container.cardPadding');\n        if(cardPadding > 0){\n          cardPadding = cardPadding - 1;\n        }\n        [cardPadding, 0, cardPadding, 10]\n       }"
            ]
          }
        }
      ]
    },
    "defaultPrimaryButton": {
      "color": {
        "#ref": [
          "masterConfig.themes.Colors.primaryColor"
        ]
      },
      "width": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          272,
          "match_parent"
        ]
      },
      "height": 48,
      "cornerRadius": {
        "#ref": [
          "masterConfig.components.buttons.cornerRadius"
        ]
      },
      "margin": {
        "#js-expr": [
          "if (window.isDesktopView()) {\n        [0, 10, 0,0]\n      } else {\n        rc('masterConfig.components.buttons.margin');\n        }"
        ]
      },
      "stroke": {
        "#js-expr": [
          "var strokeColor = rc('masterConfig.components.buttons.strokeColor');\n                  var strokeWidth = rc('masterConfig.components.buttons.strokeWidth');\n                  var isVisible = rc('masterConfig.components.buttons.strokeVisibility');\n                  isVisible == false ? '' : strokeWidth+\",\"+strokeColor"
        ]
      },
      "text": {
        "text": {
          "#ref": [
            "masterConfig.components.buttons.text"
          ]
        },
        "color": {
          "#ref": [
            "masterConfig.components.buttons.fontColor"
          ]
        },
        "size": {
          "#ref": [
            "masterConfig.components.buttons.fontSize"
          ]
        }
      }
    },
    "ppAmountBar": {
      "#override": [
        "defaultAmountBar",
        {
          "visibility": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.visible"
                ]
              },
              "VISIBLE",
              "GONE"
            ]
          }
        }
      ]
    },
    "defaultWebAmountBar": {
      "padding": [
        0,
        20,
        0,
        20
      ],
      "percentWidth": true,
      "width": 94,
      "height": 85,
      "leftSection": {
        "size": 24,
        "font": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        },
        "color": {
          "#ref": [
            "masterConfig.desktopView.OrderSummary.orderSummaryHeader.fontColor"
          ]
        }
      },
      "lineItems": [
        {
          "leftText": {
            "text": "Subscription type",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              82,
              0,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": "Amount",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              151,
              0,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          }
        },
        {
          "leftText": {
            "text": {
              "#js-expr": [
                "var text = \"\";\n                          try {\n                            var orderDetails = JSON.parse(window.__payload.payload.orderDetails);\n                            var description = JSON.parse(orderDetails.description);\n                            text =  description ? description : \"Empty\";\n                          } catch (e) {\n                            text = \"Empty\";\n                          }\n                          text;"
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#ref": [
                "globalConfig.fontSizeVeryLarge"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.orderNameText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              81,
              5,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": "<amount>",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "globalConfig.fontSizeVeryLarge"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "1,#EBEBEB,r",
            "minWidth": 200,
            "padding": [
              153,
              5,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          }
        }
      ],
      "translation": {
        "#if": [
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.visible"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.nativeShadow"
            ]
          },
          0
        ]
      },
      "background": {
        "#ref": [
          "masterConfig.desktopView.OrderSummary.backgroundColor"
        ]
      },
      "visibility": {
        "#if": [
          {
            "#ref": [
              "masterConfig.desktopView.OrderSummary.visible"
            ]
          },
          "VISIBLE",
          "GONE"
        ]
      }
    },
    "defaultAmountBar": {
      "padding": [
        16,
        8,
        16,
        8
      ],
      "margin": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.layout"
                ]
              },
              "boxed"
            ]
          },
          [
            20,
            10,
            20,
            10
          ],
          [
            0,
            0,
            0,
            0
          ]
        ]
      },
      "cornerRadius": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.layout"
                ]
              },
              "boxed"
            ]
          },
          {
            "#ref": [
              "masterConfig.components.OrderSummary.cornerRadius"
            ]
          },
          0
        ]
      },
      "rightSection": {
        "visibility": "GONE"
      },
      "lineItems": [
        {
          "leftText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "",
                {
                  "#if": [
                    {
                      "#eq": [
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.containerStyle"
                          ]
                        },
                        2
                      ]
                    },
                    "This is your order name",
                    "Plan Type"
                  ]
                }
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontSize"
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.labelText.fontSize"
                  ]
                }
              ]
            },
            "color": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontColor"
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.labelText.fontColor"
                  ]
                }
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              2
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "This is your order name",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.orderNameText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.orderNameText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "rightText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    4
                  ]
                },
                "<amount>",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              2
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "padding": [
            0,
            0,
            0,
            0
          ]
        },
        {
          "leftText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                "<amount>",
                {
                  "#js-expr": [
                    "var text = \"\";\nvar type = rc('masterConfig.components.OrderSummary.containerStyle')\n                        try {\n                          var orderDetails = JSON.parse(window.__payload.payload.orderDetails);\n                          var description = JSON.parse(orderDetails.description);\n                          text =  description ? description : \"This is your order name\";\n                        } catch (e) {\n                          text = \"This is your order name\";\n                        }\n                        type == 3 ? '' : text;"
                  ]
                }
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.amountText.fontSize"
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontSize"
                  ]
                }
              ]
            },
            "color": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    4
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontColor"
                  ]
                },
                {
                  "#if": [
                    {
                      "#eq": [
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.containerStyle"
                          ]
                        },
                        1
                      ]
                    },
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.orderNameText.fontColor"
                      ]
                    },
                    {
                      "#if": [
                        {
                          "#eq": [
                            {
                              "#ref": [
                                "masterConfig.components.OrderSummary.containerStyle"
                              ]
                            },
                            2
                          ]
                        },
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.amountText.fontColor"
                          ]
                        },
                        {
                          "#ref": [
                            "globalConfig.textPrimaryColor"
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "<amount>",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "rightText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    1
                  ]
                },
                "<amount>",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "padding": [
            0,
            0,
            0,
            0
          ]
        }
      ],
      "translation": {
        "#if": [
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.visible"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.nativeShadow"
            ]
          },
          0
        ]
      },
      "background": {
        "#ref": [
          "masterConfig.components.OrderSummary.containerColor"
        ]
      },
      "visibility": "GONE",
      "dividerColor": "#CCCCCC"
    },
    "webBackToolBar": {
      "#override": [
        "webPaymentHeaderToolbar",
        {
          "background": "#ffffff",
          "textSize": {
            "#js-expr": [
              "rc('globalConfig.fontSize')"
            ]
          },
          "leftIcon.visibility": "Visible",
          "visibility": "VISIBLE",
          "contentMargin": [
            25,
            0,
            0,
            0
          ]
        }
      ]
    },
    "webPaymentHeaderToolbar": {
      "#override": [
        "defaultToolbar",
        {
          "background": {
            "#ref": [
              "screenConfig.bgPrimaryColor"
            ]
          },
          "textSize": {
            "#js-expr": [
              "rc('globalConfig.fontSize') + 4"
            ]
          },
          "leftIcon.visibility": "GONE",
          "text": "Payment Methods",
          "visibility": {
            "#js-expr": [
              "var visibility = \"GONE\"; try{\n  visibility = (window.__OS.toLowerCase() === \"web\") ? (window.__payload.integrationType.toLowerCase() === \"iframe\") ?\n  \"GONE\" : \"GONE\" : \"GONE\";\n} catch(err){\n} visibility;"
            ]
          }
        }
      ]
    },
    "ppToolbar": {
      "#override": [
        "defaultToolbar",
        {
          "visibility": {
            "#js-expr": [
              "var visibility = \"VISIBLE\"; try{\n  visibility = (window.__OS.toLowerCase() === \"web\") ? (window.__payload.integrationType.toLowerCase() === \"iframe\") ?\n  \"VISIBLE\" : \"VISIBLE\" : \"VISIBLE\";\n} catch(err){\n} visibility;"
            ]
          }
        }
      ]
    },
    "defaultToolbar": {
      "background": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          "#ffffff",
          {
            "#ref": [
              "masterConfig.components.appBar.fillColor"
            ]
          }
        ]
      },
      "text": "Payment Methods",
      "textColor": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          {
            "#ref": [
              "globalConfig.textPrimaryColor"
            ]
          },
          {
            "#ref": [
              "masterConfig.components.appBar.fontColor"
            ]
          }
        ]
      },
      "textSize": {
        "#ref": [
          "masterConfig.components.appBar.fontSize"
        ]
      },
      "textGravity": "LEFT",
      "padding": {
        "#ref": [
          "masterConfig.components.appBar.padding"
        ]
      },
      "imageUrl": {
        "#js-expr": [
          "(window.getIcons && JSON.parse(window.getIcons()).toolbarBackArrow) ? JSON.parse(window.getIcons()).toolbarBackArrow : 'toolbar_back_arrow'"
        ]
      },
      "contentMargin": {
        "#js-expr": [
          "var uiPadding = rc('screenConfig.uiCard.horizontalPadding');\n                  var topMargin = rc('flowConfig.drawFromStatusBar') ? window.getStatusBarHeight() : 0;\n                  [13, 0, 0, 0]"
        ]
      },
      "translation": {
        "#if": [
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.visible"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.nativeShadow"
            ]
          },
          0
        ]
      },
      "leftIcon": {
        "url": {
          "#js-expr": [
            "(window.getIcons && JSON.parse(window.getIcons()).toolbarBackArrow) ? JSON.parse(window.getIcons()).toolbarBackArrow : 'toolbar_back_arrow'"
          ]
        }
      }
    },
    "unlinkedWalletListItem": {
      "#override": [
        "defaultListItem",
        {
          "bottomView.button.height": "44",
          "bottomView.button.margin": {
            "#js-expr": [
              "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0];\n      if(window.isDesktopView()){\n        val = val + 7;\n      }\n      [val, 0, 5, 0]"
            ]
          },
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              275,
              "match_parent"
            ]
          },
          "bottomView.editText.visibility": "GONE"
        }
      ]
    },
    "upiAppListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.rightImage.visibility": "visible",
          "topView.rightImage.usePackageIcon": false,
          "topView.isClickable": true,
          "bottomView.bottomDefaultExpand": false,
          "bottomView.editText.visibility": "gone",
          "bottomView.fifthLine.visibility": "gone",
          "divider.visibility": "gone"
        }
      ]
    },
    "nbScreenOtherBanksListItem": {
      "#override": [
        "defaultListItem",
        {
          "bottomView.editText.visibility": "gone",
          "bottomView.button.width": "match_parent"
        }
      ]
    },
    "ppSavedCardListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                18,
                10,
                26,
                10
              ],
              [
                {
                  "#ref": [
                    "masterConfig.components.container.cardPadding"
                  ]
                },
                16,
                {
                  "#ref": [
                    "masterConfig.components.container.cardPadding"
                  ]
                },
                16
              ]
            ]
          },
          "topView.secondLine.visibility": "visible",
          "topView.firstLine.textOne.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                16,
                0,
                0,
                0
              ],
              [
                7,
                0,
                0,
                0
              ]
            ]
          },
          "topView.secondLine.text.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                16,
                {
                  "#ref": [
                    "masterConfig.components.listItems.spacingBetween"
                  ]
                },
                0,
                0
              ],
              [
                7,
                {
                  "#ref": [
                    "masterConfig.components.listItems.spacingBetween"
                  ]
                },
                0,
                0
              ]
            ]
          },
          "topView.secondLine.text.visibility": "visible",
          "bottomView.editTextWeight": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              0.15,
              0.25
            ]
          },
          "bottomView.buttonWeight": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              0.85,
              0.75
            ]
          },
          "bottomView.button.margin": {
            "#js-expr": [
              "if (window.isDesktopView()) {\n                [20, 0, 0, 0]\n              } else {\n                [10, 0, 2, 0]\n              }"
            ]
          },
          "bottomView.editText.hint.text": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              "C V V",
              "●●●"
            ]
          },
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              250,
              "match_parent"
            ]
          },
          "bottomView.button.height": "44",
          "bottomView.editText.input.height": "44",
          "bottomView.padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\nvar cardPadding = rc('masterConfig.components.container.cardPadding');\n      var val = padding[0] + cardPadding + 21;\n      [val, 0, 0, 16]"
                ]
              },
              {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0] + 13;\n      [val, 0, 4, 16]"
                ]
              }
            ]
          },
          "bottomView.editText.input.padding": [
            10,
            4,
            10,
            4
          ],
          "bottomView.editText.focus": true,
          "bottomView.editText.header.text": "CVV",
          "bottomView.editText.header.color": {
            "#ref": [
              "masterConfig.components.inputFields.activeStateColor"
            ]
          },
          "bottomView.editText.header.padding": {
            "#ref": [
              "masterConfig.components.inputFields.fieldName.padding"
            ]
          },
          "bottomView.editText.stroke": {
            "#if": [
              {
                "#eq": [
                  {
                    "#ref": [
                      "masterConfig.components.inputFields.type"
                    ]
                  },
                  "underline"
                ]
              },
              "",
              {
                "#js-expr": [
                  "var color = rc(\"masterConfig.components.inputFields.activeStateColor\"); \"1,\"+ color"
                ]
              }
            ]
          },
          "bottomView.editText.input.letterSpacing": 1,
          "bottomView.editText.input.textSize": 16,
          "bottomView.editText.input.width": 55,
          "bottomView.editText.input.inpType": "NumericPassword",
          "bottomView.editText.icon.visibility": "GONE",
          "bottomView.fifthLine.visibility": "gone"
        }
      ]
    },
    "savedVPAListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.firstLine.textTwo.visibility": "gone",
          "topView.secondLine.visibility": "visible",
          "topView.secondLine.text.visibility": "visible",
          "bottomView.editText.visibility": "gone",
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              275,
              "match_parent"
            ]
          }
        }
      ]
    },
    "linkedWalletListItem": {
      "#override": [
        "defaultListItem",
        {
          "bottomView.button.height": "44"
        }
      ]
    },
    "ppSavedVPAListItem": {
      "#override": [
        "defaultListItem",
        {
          "bottomView.button.height": "44",
          "topView.secondLine.text.visibility": "VISIBLE",
          "topView.secondLine.visibility": "VISIBLE",
          "bottomView.editText.visibility": "GONE"
        }
      ]
    },
    "defaultListItem": {
      "topView": {
        "firstLine": {
          "textOne": {
            "color": {
              "#ref": [
                "masterConfig.components.listItems.mainText.fontColor"
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.listItems.mainText.fontSize"
              ]
            },
            "margin": [
              7,
              0,
              0,
              0
            ],
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          },
          "imageOne": {
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          }
        },
        "secondLine": {
          "text": {
            "color": {
              "#ref": [
                "masterConfig.components.listItems.subText.fontColor"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.listItems.subText.fontSize"
              ]
            },
            "margin": [
              7,
              {
                "#ref": [
                  "masterConfig.components.listItems.spacingBetween"
                ]
              },
              0,
              0
            ],
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          }
        },
        "leftImage": {
          "size": {
            "#ref": [
              "masterConfig.components.listItems.primaryIconSize"
            ]
          },
          "margin": [
            0,
            0,
            0,
            0
          ],
          "padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                5,
                5,
                5,
                5
              ],
              [
                0,
                0,
                0,
                0
              ]
            ]
          }
        },
        "rightImage": {
          "size": {
            "#ref": [
              "masterConfig.components.listItems.secondaryIconSize"
            ]
          }
        },
        "selectionLabel": {
          "size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "font": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          },
          "background": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              "#ffffff",
              {
                "#ref": [
                  "masterConfig.themes.Colors.defaultTileColor"
                ]
              }
            ]
          },
          "cornerRadius": 2,
          "stroke": ""
        },
        "height": 68,
        "padding": [
          {
            "#js-expr": [
              "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
            ]
          },
          16,
          {
            "#js-expr": [
              "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
            ]
          },
          16
        ]
      },
      "bottomView": {
        "editText": {
          "#override": [
            "defaultEditText",
            {
              "input.height": 48,
              "focus": true,
              "visibility": "GONE"
            }
          ]
        },
        "button": {
          "#override": [
            "defaultPrimaryButton",
            {
              "margin": {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0];\n      if(window.isDesktopView()){\n        val = val + 7;\n      }\n      [val, 0, 5, 0]"
                ]
              },
              "height": 44,
              "width": "match_parent"
            }
          ]
        },
        "margin": {
          "#js-expr": [
            "var imageSize = rc('defaultListItem.topView.leftImage.size');\n      var pX = rc('screenConfig.uiCard.horizontalPadding') - 6;\n      if (window.isDesktopView()) pX = 3;\n      var space = pX + imageSize;\n      [space, 0, pX, 0]"
          ]
        },
        "padding": {
          "#js-expr": [
            "var pY = rc('screenConfig.uiCard.horizontalPadding');\n      [12, 0, 0, 16]"
          ]
        }
      }
    },
    "cardNumberConfig": {
      "#override": [
        "defaultEditText",
        {
          "input.separator": {
            "#js-expr": [
              "' '"
            ]
          },
          "input.separatorRepeat": "4",
          "input.inpType": "numeric",
          "input.pattern": "^([0-9]| )+$,24",
          "icon.width": 40,
          "icon.height": 40
        }
      ]
    },
    "cvvConfig": {
      "#override": [
        "defaultEditText",
        {
          "input.inpType": "NumericPassword",
          "input.pattern": "^[0-9]+$,3",
          "visibility": "VISIBLE",
          "icon.visibility": "visible",
          "icon.width": {
            "#ref": [
              "globalConfig.checkboxSize"
            ]
          },
          "icon.height": {
            "#ref": [
              "globalConfig.checkboxSize"
            ]
          }
        }
      ]
    },
    "expiryDateConfig": {
      "#override": [
        "defaultEditText",
        {
          "hint.text": "MM / YY",
          "input.separator": "/",
          "input.separatorRepeat": "2",
          "input.inpType": "Numeric",
          "input.pattern": "^([0-9]|\\/)+$,5",
          "visibility": "VISIBLE",
          "icon.textVisibility": "gone",
          "icon.visibility": "visible",
          "input.width": "wrap_content",
          "error.textFont": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          }
        }
      ]
    }
  }
};
      return JSON.stringify(configuration);
      }