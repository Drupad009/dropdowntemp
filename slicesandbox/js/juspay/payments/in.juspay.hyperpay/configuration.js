window.hyperpay_configuration.js_version = 2.0.0;
let fontName = "Slice";
    let fonts = [
  {
    "name": "Slice",
    "url": "https://assets.juspay.in/hyper/fonts/Slice-Regular.ttf",
    "weight": 400
  },
  {
    "name": "Slice",
    "url": "https://assets.juspay.in/hyper/fonts/Slice-SemiBold.ttf",
    "weight": 500
  },
  {
    "name": "Slice",
    "url": "https://assets.juspay.in/hyper/fonts/Slice-Bold.ttf",
    "weight": 700
  }
];

    if(window.JBridge && typeof window.JBridge.loadFonts === 'function') {
    window.JBridge.loadFonts(fonts);
    }

    window.getMerchantConfig = function () {
      var configuration = {
  "componentMapping": {
    "*.Global": "globalConfig",
    "*.FlowConfig": "flowConfig",
    "*.ScreenConfig": "screenConfig",
    "*.EMIInstrumentsScreen.ListItem": "emiInstrumentListItem",
    "*.EMIStoredCard.ListItem": "emiStoredCardListItem",
    "*.EMIPlansScreen.ScreenConfig": "emiPlansScreenConfig",
    "*.EMICheckoutScreen.ScreenConfig": "emiCheckoutScreenConfig",
    "*.EMIOptionsScreen.ScreenConfig": "emiOptionsScreenConfig",
    "*.EMIAmountWidgetScreen.ScreenConfig": "emiPlansScreenConfig",
    "*.Toolbar": "defaultToolbar",
    "*.AmountBar": "defaultAmountBar",
    "*.WebWrapper.AmountBar": "defaultWebAmountBar",
    "*.WebWrapper.PaymentHeader.Toolbar": "defaultWebPaymentHeaderToolbar",
    "*.PrimaryButton": "defaultPrimaryButton",
    "*.Message": "defaultMessage",
    "*.EditText": "defaultEditText",
    "*.ListItem": "defaultListItem",
    "*.GridItem": "defaultGridItem",
    "*.SearchBox": "defaultSearchBox",
    "*.NavBar": "defaultNavBar",
    "*.AddCard": "defaultAddCard",
    "*.EMICheckoutScreen.AddCard": "emiCheckoutAddCard",
    "*.Popup": "defaultPopup",
    "*.SecondaryButton": "defaultSecondaryButton",
    "*.PrestoList": "defaultPrestoList",
    "*.Loader": "defaultLoaderConfig",
    "*.SavedCard.ListItem": "savedCardListItem",
    "*.EMIPlansScreen.ListItem": "emiPlansListItem",
    "*.EMIOptionsScreen.ListItem": "emiOptionsListItem",
    "*.EMICheckoutScreenInstrument.ListItem": "emiCheckoutListItemInstrument",
    "*.PaymentOption.ListItem": "paymentOptionListItem",
    "*.PaymentOption.GenericIntent.ListItem": "paymentOptionGenericIntentListItem",
    "*.AddButton.Toolbar": "webAddButtonToolbar",
    "*.Error.Message": "errorMessage",
    "*.ExpandedViews.ListItem": "expandedViewsListItem",
    "*.OtherBanks.ListItem": "otherBanksListItem",
    "*.SavedVPA.ListItem": "ppSavedVPAListItem",
    "*.InApp.ListItem": "inAppListItem",
    "*.Rewards.ListItem": "rewardsListItem",
    "*.WalletVerifyNumberScreen.EditText": "verifyNumberEditText",
    "*.WalletVerifyOTPScreen.EditText": "verifyOtpEditText",
    "*.PaymentOption.FoodCards.ListItem": "paymentOptionFoodCardsListItem",
    "*.PaymentInfo.Message": "paymentInfoMessage",
    "*.PaymentBottomInfo.Message": "paymentBottomInfo",
    "*.Surcharge.Message": "surchargeMessage",
    "*.Outage.Message": "defaultMessage",
    "RewardsPopup.ScreenConfig": "rewardsScreenConfig",
    "EnableSI.Message": "enableSIBar",
    "RewardsPay.Message": "rewardsPayMessage",
    "SaveDefault.Message": "defaultOptionBar",
    "MandateEducation.PrimaryButton": "mandateEducationPrimaryButton",
    "RewardsEducation.PrimaryButton": "rewardsEducationPrimaryButton",
    "*.OtherUPI.SecondaryButton": "otherUPISecondaryButton",
    "NBScreen.PrimaryButton": "nbPrimaryButton",
    "SingleCard.Message": "singleCardMessage",
    "Screen.Popup": "deletePopupConfig",
    "PaymentManagementScreen.Popup": "deletePopupConfig",
    "PaymentPageScreen.PaymentOption.PaymentManagement.ListItem": "pmListItem",
    "PaymentStatus.Popup": "paymentStatusPopup",
    "RetrySuggestion.ListItem": "retrySuggestionListItem",
    "ViesEnrollment.Popup": "viesEnrollmentPopupConfig",
    "BackPressDialog.Popup": "backPressDialogPopup",
    "WalletScreen.UnLinked.ListItem": "unlinkedWalletListItem",
    "WalletScreen.Linked.ListItem": "linkedWalletListItem",
    "PaymentManagement.SavedCard.ListItem": "pmSavedCardListItem",
    "PaymentManagement.SavedVPA.ListItem": "pmSavedVPAListItem",
    "QuickPayScreen.Linked.ListItem": "quickPayLinkedWallet",
    "QuickPayScreen.SavedCard.ListItem": "quickPaySavedCard",
    "QuickPayScreen.NetBank.ListItem": "quickPayNB",
    "QuickPayScreen.UpiCollect.ListItem": "quickPayUpiCollect",
    "QuickPayScreen.UnlinkedWallets.ListItem": "quickPayUnlinkedWallet",
    "QuickPayScreen.PrimaryButton": "quickPayPrimaryButton",
    "NBScreen.MandateConsent.Message": "nbMandateConsentMessage",
    "PaymentPage.ExpandedNB.ListItem": "expandedNBBottomListItem",
    "PaymentStatusScreen.ListItem": "paymentStatusListItem",
    "PaymentStatus.AmountBar": "paymentStatusAmountBar",
    "PaymentStatusScreen.SecondaryButton": "paymentStatusSecondaryButton",
    "PaymentStatusScreen.PrimaryButton": "paymentStatusPrimaryButton",
    "NBScreen.SearchBox": "nbScreenSearchBox",
    "*.WalletScreen.ScreenConfig": "walletScreenConfig",
    "PaymentPage.Expanded.LinkedWallet.ListItem": "linkedWalletListItem",
    "PaymentPage.Expanded.UnlinkedWallet.ListItem": "unlinkedWalletListItem",
    "UPIScreen.SavedVPA.ListItem": "savedVPAListItem",
    "PaymentPageScreen.AmountBar": "ppAmountBar",
    "PaymentPageScreen.Toolbar": "ppToolbar",
    "NBScreen.OtherBanks.SecondaryButton": "nbOtherBanksSecondaryButton",
    "UPIScreen.OtherUPI.SecondaryButton": "upiOtherOptionsSecondaryButton",
    "PaymentPage.SavedCard.ListItem": "ppSavedCardListItem",
    "NBScreen.OtherBanks.ListItem": "nbScreenOtherBanksListItem",
    "UPIScreen.UPIApp.ListItem": "upiAppListItem",
    "WebWrapper.PaymentHeader.Toolbar": "webPaymentHeaderToolbar",
    "WebWrapper.Back.Toolbar": "webBackToolBar",
    "COD.ScreenConfig": "codScreen",
    "PaymentPageScreen.CashOD.ListItem": "unlinkedWalletListItem",
    "WalletOTPScreen.ScreenConfig": "linkWalletScreenConfig",
    "NBScreen.ScreenConfig": "nbScreen",
    "AddCardScreen.Surcharge.Message": "addCardSurchargeMessage"
  },
  "mainConfig": {
    "globalConfig": {
      "primaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.primaryColor"
        ]
      },
      "secondaryColor": "#D6D6D6",
      "textPrimaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.defaultTextColor"
        ]
      },
      "textSecondaryColor": "#999999",
      "textTertiaryColor": "#999999",
      "errorColor": {
        "#ref": [
          "masterConfig.themes.Colors.errorColor"
        ]
      },
      "successColor": {
        "#ref": [
          "masterConfig.themes.Colors.successColor"
        ]
      },
      "dividerColor": "#e9e9e9",
      "hintColor": "#999999",
      "checkboxFontColor": "#6B6B6B",
      "primaryFont": {
        "type": "FontName",
        "value": "Slice"
      },
      "checkboxFont": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Regular\";"
          ]
        }
      },
      "fontBold": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Bold\";"
          ]
        }
      },
      "fontSemiBold": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-SemiBold\";"
          ]
        }
      },
      "fontRegular": {
        "type": "FontName",
        "value": {
          "#js-expr": [
            "  var fontType = rc('masterConfig.themes.TypoGraphy.fontFamily');\n      fontType + \"-Regular\";"
          ]
        }
      },
      "checkboxSize": 24,
      "fontSize": {
        "#ref": [
          "masterConfig.themes.TypoGraphy.fontBaseSize"
        ]
      },
      "addDivider": false
    },
    "flowConfig": {
      "showSavedVPAs": false,
      "upiQREnable": true,
      "popularBanks": [
        "NB_SBI",
        "NB_HDFC",
        "NB_ICICI",
        "NB_AXIS"
      ],
      "paymentOptions": [
        {
          "group": "others",
          "po": "wallets",
          "onlyDisable": [
            "GOOGLEPAY",
            "CRED"
          ],
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "po": "inApps",
          "onlyDisable": [],
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "onlyDisable": [],
          "po": "emi",
          "visibility": "GONE"
        },
        {
          "group": "others",
          "onlyDisable": [
            "SHAREit",
            "WhatsApp"
          ],
          "po": "upi",
          "visibility": "GONE"
        },
        {
          "group": "others",
          "onlyDisable": [
            "SHAREit",
            "WhatsApp"
          ],
          "po": "upiAppsWithOther",
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "onlyDisable": [],
          "po": "cards",
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "onlyDisable": [
            "NB_DUMMY",
            "NB_SBM",
            "NB_SBT"
          ],
          "po": "nb",
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "onlyDisable": [],
          "po": "cashod",
          "visibility": "GONE"
        },
        {
          "group": "others",
          "po": "googlepay",
          "onlyDisable": [],
          "visibility": "VISIBLE"
        },
        {
          "group": "others",
          "onlyDisable": [],
          "po": "foodCards",
          "visibility": "GONE"
        },
        {
          "group": "others",
          "onlyDisable": [],
          "po": "payLater",
          "visibility": "VISIBLE"
        }
      ],
      "sideBarTabs": {
        "#if": [
          "window.__payload.action !== 'paymentManagement'",
          {
            "#ref": [
              "flowConfig.sideBarTabsRef"
            ]
          },
          {
            "#js-expr": [
              "rc('flowConfig.sideBarTabsRef').includes('MANAGE') ? ['MANAGE']:[]"
            ]
          }
        ]
      },
      "firstLoadSideBarTab": {
        "#js-expr": [
          "rc('flowConfig.sideBarTabsRef')[0]"
        ]
      },
      "flows": {
        "enforceSaveCard": false
      },
      "payeeName": "Juspay",
      "drawFromStatusBar": false,
      "upiConfig": {
        "skipHomeScreen": true
      },
      "verifyVpa": true,
      "offers": {
        "isEnabled": {
          "#ref": [
            "masterConfig.components.offers.visible"
          ]
        },
        "isInstantDiscount": true
      },
      "enableSurcharge": true,
      "flowConfigValues": {
        "blockedMethods": {
          "netBanking": [],
          "cardBrands": [],
          "cardTypes": [
            "CREDIT"
          ],
          "cardBins": [],
          "indianCardsOnly": false,
          "upiCollectBlock": false,
          "errorMsg": "",
          "fadeOutBlockedCards": [],
          "specifiedErrorMessage": {
            "brandError": "",
            "binError": "",
            "countryError": "",
            "typeError": "<> card can't be used"
          }
        },
        "isViesAllowed": "",
        "maxViesAmount": 0
      },
      "sideBarTabsRef": [
        "CARD",
        "WALLET",
        "UPI",
        "NET_BANKING",
        "INAPPS",
        "PAY_LATER",
        "COD",
        "EMI"
      ]
    },
    "nbScreen": {
      "#override": [
        "screenConfig",
        {
          "containerAttribs.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                24,
                10,
                0,
                24
              ],
              {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.container.containerPadding');\npadding[3] = padding[3] + 40;\npadding;"
                ]
              }
            ]
          }
        }
      ]
    },
    "linkWalletScreenConfig": {
      "#override": [
        "screenConfig",
        {
          "uiCard.verticalPadding": 10
        }
      ]
    },
    "codScreen": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.text": "Pay On Delivery"
        }
      ]
    },
    "walletScreenConfig": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                26,
                26,
                0,
                10
              ],
              {
                "#ref": [
                  "masterConfig.components.container.sectionHeaderPadding"
                ]
              }
            ]
          },
          "containerAttribs.horizontalSpacing": {
            "#js-expr": [
              " var padding = rc('masterConfig.components.container.containerPadding');\n     padding[0];"
            ]
          }
        }
      ]
    },
    "emiOptionsScreenConfig": {
      "#override": [
        "screenConfig",
        {
          "sectionHeader.text": "EMI Options",
          "utils.contentMargin": {
            "#js-expr": [
              " var vSpace = rc('screenConfig.containerAttribs.verticalSpacing');\n            var hSpace = rc('screenConfig.containerAttribs.horizontalSpacing') + 12;\n            if(rc('screenConfig.containerAttribs.horizontalSpacing') == 0){\n              window.isDesktopView() ? [2,0,0,0] : rc('masterConfig.components.container.containerPadding')\n            }\n            else{\n              window.isDesktopView() ? [2,0,0,0] : [100, vSpace, hSpace, vSpace]\n            }\n          "
            ]
          }
        }
      ]
    },
    "screenConfig": {
      "bgPrimaryColor": {
        "#ref": [
          "masterConfig.themes.Colors.backgroundColor"
        ]
      },
      "bgSecondaryColor": "#FDFDFD",
      "containerAttribs": {
        "horizontalSpacing": 0,
        "verticalSpacing": 0,
        "sectionSpacing": {
          "#ref": [
            "masterConfig.components.container.cardMargin"
          ]
        },
        "margin": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              24,
              10,
              0,
              24
            ],
            {
              "#ref": [
                "masterConfig.components.container.containerPadding"
              ]
            }
          ]
        }
      },
      "utils": {
        "contentMargin": [
          0,
          0,
          0,
          0
        ],
        "sectionMargin": [
          0,
          0,
          0,
          {
            "#ref": [
              "masterConfig.components.container.cardMargin"
            ]
          }
        ]
      },
      "uiCard": {
        "translation": 0,
        "cornerRadius": {
          "#ref": [
            "masterConfig.components.container.cornerRadius"
          ]
        },
        "horizontalPadding": {
          "#js-expr": [
            "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 0 : padding"
          ]
        },
        "verticalPadding": 0,
        "color": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            "#ffffff",
            {
              "#ref": [
                "masterConfig.themes.Colors.defaultTileColor"
              ]
            }
          ]
        },
        "stroke": {
          "#js-expr": [
            "var strokeColor = rc('masterConfig.components.container.strokeColor');\nvar strokeWidth = rc('masterConfig.components.container.strokeWidth');\n                  window.isDesktopView() ? '' : strokeWidth+\",\"+strokeColor"
          ]
        },
        "addStrokeToForm": true,
        "shadow": {
          "spread": {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.spread"
            ]
          },
          "blur": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.blur"
                ]
              },
              0
            ]
          },
          "opacity": 0,
          "hOffset": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.xOffset"
                ]
              },
              0
            ]
          },
          "vOffset": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.yOffset"
                ]
              },
              0
            ]
          },
          "color": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.visible"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Shadow.cardShadow.color"
                ]
              },
              {
                "#ref": [
                  "masterConfig.themes.Colors.backgroundColor"
                ]
              }
            ]
          }
        }
      },
      "button": {
        "background": {
          "#ref": [
            "screenConfig.bgPrimaryColor"
          ]
        },
        "maxWidth": {
          "#js-expr": [
            "(window.__OS.toLowerCase() === \"web\") && window.isDesktopView() ? 223 : \"match_parent\""
          ]
        }
      },
      "sectionHeader": {
        "font": {
          "#js-expr": [
            "if (window.isDesktopView()) {\n        rc('globalConfig.fontBold')\n      } else {\n        rc('globalConfig.fontBold')\n      }"
          ]
        },
        "textSize": 16,
        "margin": {
          "#js-expr": [
            " if (window.isDesktopView()) {\n            [26, 24, 24, 12]\n          } else {\n            var uiPadding = rc('screenConfig.uiCard.horizontalPadding');\n            var hSpace = rc('screenConfig.containerAttribs.horizontalSpacing');\n            var bottomMargin = rc('screenConfig.sectionHeader.bottomMargin');\n            var tM = (rc('screenConfig.uiCard.translation') == 0.0) ? 0 : 4\n            if (hSpace == 0){\n              [0, 0, uiPadding, 0]\n            }\n            else {\n              [0, 0, 0, 0]\n            }\n          }\n        "
          ]
        },
        "padding": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              0,
              0,
              18,
              10
            ],
            {
              "#ref": [
                "masterConfig.components.container.sectionHeaderPadding"
              ]
            }
          ]
        },
        "dividerHeight": 1,
        "dividerColor": {
          "#ref": [
            "masterConfig.components.separator.color"
          ]
        },
        "dividerVisibility": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            "GONE",
            {
              "#if": [
                {
                  "#ref": [
                    "masterConfig.components.separator.visible"
                  ]
                },
                "VISIBLE",
                "GONE"
              ]
            }
          ]
        },
        "color": "#444444",
        "background": "transparent"
      },
      "sideBar": {
        "background": "#f8f8f8",
        "icon": {
          "selectedColor": {
            "#ref": [
              "masterConfig.desktopView.icon.selectedColor"
            ]
          },
          "notSelectedColor": {
            "#ref": [
              "masterConfig.desktopView.icon.notSelectedColor"
            ]
          }
        },
        "navbarItem": {
          "selectedBackgroundColor": {
            "#ref": [
              "masterConfig.desktopView.sideBar.selectedBackgroundColor"
            ]
          }
        }
      },
      "expand": {
        "walletView": {
          "#ref": [
            "masterConfig.expand.walletView"
          ]
        },
        "popularNBView": true,
        "cod": true
      },
      "nb": {
        "popularBanksBanksHeader": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            false,
            true
          ]
        },
        "gridViewPadding": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            [
              15,
              8,
              8,
              8
            ],
            [
              8,
              8,
              8,
              8
            ]
          ]
        },
        "addMargin": false,
        "showPopular": true
      },
      "upi": {
        "showAddUpiHeader": true
      },
      "card": {
        "screenHeaderTextConfig": {
          "margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                28,
                10,
                0,
                10
              ],
              [
                0,
                0,
                0,
                0
              ]
            ]
          }
        }
      },
      "sideBar.navbarItem.selectedStroke": {
        "#js-expr": [
          "\"6,\" + rc('globalConfig.primaryColor') + \",l\""
        ]
      }
    },
    "masterConfig": {
      "themes": {
        "Colors": {
          "primaryColor": "#634CBB",
          "backgroundColor": "#ffffff",
          "errorColor": "#d76559",
          "successColor": "#ffffff",
          "defaultTextColor": "#333333",
          "defaultTileColor": "#ffffff"
        },
        "TypoGraphy": {
          "fontBaseSize": 16,
          "fontFamily": "Slice"
        },
        "Shadow": {
          "cardShadow": {
            "visible": false,
            "blur": 30,
            "yOffset": 8,
            "xOffset": 4,
            "spread": 2,
            "nativeShadow": 0,
            "color": "#000000"
          }
        }
      },
      "components": {
        "buttons": {
          "fillColor": {
            "#ref": [
              "masterConfig.themes.Colors.primaryColor"
            ]
          },
          "strokeColor": "#000000",
          "strokeWidth": "0",
          "strokeVisibility": false,
          "cornerRadius": 24,
          "fontColor": "#ffffff",
          "fontSize": 13,
          "fontWeight": "Medium",
          "text": "Proceed to Pay",
          "margin": [
            0,
            15,
            0,
            0
          ]
        },
        "searchBox": {
          "leftImage": {
            "size": 16,
            "margin": [
              0,
              0,
              5,
              0
            ]
          },
          "nonActiveState": {
            "strokeColor": "#dddddd"
          }
        },
        "inputFields": {
          "type": "box",
          "disabledStateColor": "#E9E9E9",
          "activeStateColor": "#000000",
          "fieldName": {
            "fontColor": "#555555",
            "fontSize": 10,
            "fontWeight": "bold",
            "padding": [
              0,
              0,
              0,
              0
            ]
          },
          "inputText": {
            "fontColor": "#333333",
            "fontSize": 14,
            "fontWeight": "bold",
            "padding": [
              10,
              0,
              10,
              0
            ]
          }
        },
        "container": {
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "strokeColor": "#ffffff",
          "strokeWidth": 1,
          "cornerRadius": 0,
          "sectionHeaderPadding": [
            8,
            0,
            0,
            0
          ],
          "containerPadding": [
            22,
            20,
            22,
            15
          ],
          "cardMargin": 32,
          "cardPadding": 5
        },
        "links": {
          "fontColor": "#624DC2",
          "fontSize": 15,
          "fontWeight": "bold",
          "marginUpi": 14,
          "marginNb": 14
        },
        "offers": {
          "containerColor": "#ffccff",
          "containerRadius": 12,
          "visible": false,
          "otherIcons": {
            "iconColor": "#ffccff",
            "fontColor": "#ffccff",
            "fontSize": 12,
            "fontWeight": "bold",
            "padding": [
              1,
              0,
              0,
              1
            ]
          }
        },
        "separator": {
          "color": "#d0021b",
          "visible": false
        },
        "grid": {
          "fontColor": "#000000",
          "fontSize": 13,
          "fontWeight": "bold",
          "iconSize": 36
        },
        "listItems": {
          "paddings": [
            0,
            0,
            0,
            0
          ],
          "mainText": {
            "fontColor": {
              "#ref": [
                "masterConfig.themes.Colors.defaultTextColor"
              ]
            },
            "fontSize": {
              "#ref": [
                "masterConfig.themes.TypoGraphy.fontBaseSize"
              ]
            },
            "fontWeight": "bold"
          },
          "subText": {
            "fontColor": "#777777",
            "fontSize": 12,
            "fontWeight": "bold"
          },
          "spacingBetween": 0,
          "primaryIconSize": 42,
          "secondaryIconSize": 20
        },
        "appBar": {
          "fillColor": "#ffffff",
          "padding": [
            16,
            0,
            0,
            0
          ],
          "fontColor": "#333333",
          "fontSize": 18,
          "fontWeight": "bold"
        },
        "OrderSummary": {
          "visible": true,
          "layout": "boxe",
          "containerStyle": 1,
          "containerColor": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 10,
          "labelText": {
            "fontColor": "#999999",
            "fontSize": 12
          },
          "orderNameText": {
            "fontColor": "#0000FF",
            "fontSize": 15
          },
          "amountText": {
            "fontColor": "#000000",
            "fontSize": 15
          },
          "spacingBetween": 0
        }
      },
      "desktopView": {
        "OrderSummary": {
          "visible": true,
          "backgroundColor": "#ffffff",
          "orderSummaryHeader": {
            "fontColor": "#363636"
          },
          "labelText": {
            "fontColor": "#999999",
            "fontSize": 12
          },
          "orderNameText": {
            "fontColor": "#363636"
          },
          "amountText": {
            "fontColor": "#363636"
          }
        },
        "icon": {
          "selectedColor": "#000000",
          "notSelectedColor": "#354052"
        },
        "sideBar": {
          "selectedBackgroundColor": "#FF0000"
        }
      },
      "expand": {
        "walletView": true
      }
    },
    "defaultLoaderConfig": {
      "dot": {
        "background": "#ffffff"
      },
      "text": {
        "color": "#FF0000"
      }
    },
    "defaultPrestoList": {
      "padding": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            0,
            0,
            24,
            0
          ],
          {
            "#js-expr": [
              "var hSpace = rc('masterConfig.components.container.cardPadding');\n[hSpace, 0, hSpace, 0]"
            ]
          }
        ]
      },
      "margin": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            25,
            0,
            28,
            0
          ],
          [
            0,
            0,
            0,
            0
          ]
        ]
      },
      "font": {
        "#ref": [
          "globalConfig.fontRegular"
        ]
      },
      "textSize": {
        "#ref": [
          "masterConfig.components.listItems.mainText.fontSize"
        ]
      },
      "leftImage": {
        "size": {
          "#ref": [
            "masterConfig.components.listItems.primaryIconSize"
          ]
        }
      },
      "rightImage": {
        "size": {
          "#ref": [
            "masterConfig.components.listItems.secondaryIconSize"
          ]
        }
      },
      "hasButton": false,
      "space": 16
    },
    "upiOtherOptionsSecondaryButton": {
      "#override": [
        "defaultSecondaryButton",
        {
          "text.text": "Add UPI ID",
          "margin": [
            {
              "#ref": [
                "masterConfig.components.links.marginUpi"
              ]
            },
            0,
            0,
            10
          ],
          "text.size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "text.color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 2,
          "stroke": "",
          "text.padding": [
            0,
            2,
            4,
            2
          ],
          "height": 30,
          "width": 130
        }
      ]
    },
    "nbOtherBanksSecondaryButton": {
      "#override": [
        "defaultSecondaryButton",
        {
          "text.text": "Other Banks",
          "margin": [
            {
              "#ref": [
                "masterConfig.components.links.marginNb"
              ]
            },
            0,
            0,
            10
          ],
          "text.size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "text.color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          },
          "cornerRadius": 2,
          "stroke": "",
          "text.padding": [
            0,
            2,
            4,
            2
          ],
          "height": 30,
          "width": 130
        }
      ]
    },
    "defaultSecondaryButton": {
      "#override": [
        "defaultPrimaryButton",
        {
          "cornerRadius": 15,
          "translation": 0,
          "margin": [
            0,
            0,
            0,
            0
          ],
          "stroke": "0,#ffffff",
          "color": {
            "#ref": [
              "screenConfig.uiCard.color"
            ]
          },
          "text.color": {
            "#ref": [
              "globalConfig.primaryColor"
            ]
          },
          "text.size": 14,
          "text.font": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          },
          "image.visibility": "VISIBLE",
          "image.url": "https://assets.juspay.in/hyper/images/slice/jp_right_arrow_small.png",
          "image.size": 15
        }
      ]
    },
    "defaultAddCard": {
      "cardConfig": {
        "padding": {
          "#js-expr": [
            " var hP = rc('screenConfig.uiCard.horizontalPadding');\n        var vP = rc('screenConfig.uiCard.verticalPadding');\n        if (window.isDesktopView()) {\n          [11, 0, 0, 0]\n        } else {\n          [hP, vP, hP, vP]\n        }\n      "
          ]
        },
        "cornerRadius": {
          "#js-expr": [
            " var hP = rc('screenConfig.containerAttribs.horizontalSpacing');\n        if (hP == 0) {\n          rc('masterConfig.components.container.cornerRadius')\n        } else {\n          rc('masterConfig.components.container.cornerRadius')\n        }\n      "
          ]
        }
      },
      "cardNumber": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.textSecondaryColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "cardNumberConfig",
            {
              "input.height": 48
            }
          ]
        },
        "inputFieldMargin": {
          "#if": [
            {
              "#js-expr": [
                "window.__OS == 'ANDROID'"
              ]
            },
            [
              0,
              0,
              0,
              10
            ],
            [
              0,
              8,
              0,
              20
            ]
          ]
        }
      },
      "expiry": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.textSecondaryColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "expiryDateConfig",
            {
              "icon.visibility": "gone",
              "input.height": 48
            }
          ]
        }
      },
      "cvv": {
        "labelConfig": {
          "textFont": {
            "#ref": [
              "globalConfig.fontRegular"
            ]
          },
          "textColor": {
            "#ref": [
              "globalConfig.hintColor"
            ]
          },
          "textSize": {
            "#ref": [
              "globalConfig.fontSizeSmall"
            ]
          }
        },
        "editTextConfig": {
          "#override": [
            "cvvConfig",
            {
              "input.height": 48
            }
          ]
        }
      },
      "saveCard": {
        "text": "Securely save this card for future payments.",
        "textSize": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            {
              "#ref": [
                "globalConfig.fontSizeSmall"
              ]
            },
            {
              "#ref": [
                "globalConfig.fontSizeVerySmall"
              ]
            }
          ]
        },
        "textFont": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        },
        "margin": [
          0,
          0,
          0,
          0
        ],
        "infoIcon": {
          "visible": false,
          "size": 20
        }
      },
      "payButtonConfig": {
        "#override": [
          "defaultPrimaryButton",
          {
            "margin": {
              "#if": [
                {
                  "#js-expr": [
                    "window.isDesktopView()"
                  ]
                },
                [
                  28,
                  0,
                  0,
                  0
                ],
                {
                  "#js-expr": [
                    "var containerPadding = rc('masterConfig.components.container.containerPadding');\n                  var buttonPadding = rc('masterConfig.components.buttons.margin');\n                  var leftPadding = containerPadding[0] + buttonPadding[0];\n                  var topPadding = containerPadding[1] + buttonPadding[1];\n                  var rightPadding = containerPadding[2] + buttonPadding[2];\n                  var bottomPadding = containerPadding[3] + buttonPadding[3];\n                  [leftPadding, topPadding, rightPadding, bottomPadding];"
                  ]
                }
              ]
            },
            "width": {
              "#if": [
                {
                  "#js-expr": [
                    "window.isDesktopView()"
                  ]
                },
                250,
                "match_parent"
              ]
            }
          }
        ]
      },
      "cardConfig.margin": {
        "#js-expr": [
          "var vP = rc('screenConfig.containerAttribs.verticalSpacing');\n                  var tM = rc('screenConfig.utils.translationMargin');\n                  window.isDesktopView() ? [16, vP, tM, vP] : rc('masterConfig.components.container.containerPadding')"
        ]
      }
    },
    "defaultNavBar": {
      "background": "#F8F8F8",
      "textSize": {
        "#js-expr": [
          "rc('globalConfig.fontSize') - 1"
        ]
      },
      "selectedBackground": "#ffffff"
    },
    "nbScreenSearchBox": {
      "#override": [
        "defaultSearchBox",
        {
          "stroke": {
            "#js-expr": [
              "var strokeColor = rc('masterConfig.components.searchBox.nonActiveState.strokeColor');\nwindow.isDesktopView() ? '' : \"1,\"+strokeColor"
            ]
          }
        }
      ]
    },
    "defaultSearchBox": {
      "leftImage": {
        "size": {
          "#ref": [
            "masterConfig.components.searchBox.leftImage.size"
          ]
        },
        "margin": {
          "#ref": [
            "masterConfig.components.searchBox.leftImage.margin"
          ]
        }
      },
      "stroke": {
        "#ref": [
          "defaultEditText.stroke"
        ]
      },
      "height": 48,
      "padding": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            5,
            4,
            0,
            0
          ],
          [
            12,
            0,
            20,
            0
          ]
        ]
      },
      "margin": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            0,
            0,
            0,
            0
          ],
          [
            4,
            14,
            4,
            4
          ]
        ]
      },
      "cornerRadius": {
        "#ref": [
          "defaultEditText.cornerRadius"
        ]
      }
    },
    "defaultGridItem": {
      "size": {
        "#if": [
          {
            "#js-expr": [
              "window.parent.innerWidth >= 400"
            ]
          },
          74,
          80
        ]
      },
      "padding": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          [
            0,
            4,
            25,
            0
          ],
          [
            4,
            8,
            6,
            8
          ]
        ]
      },
      "background": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          "#ffffff",
          {
            "#ref": [
              "masterConfig.themes.Colors.defaultTileColor"
            ]
          }
        ]
      },
      "margin": [
        0,
        0,
        0,
        0
      ],
      "image": {
        "size": {
          "#ref": [
            "masterConfig.components.grid.iconSize"
          ]
        }
      },
      "text": {
        "size": {
          "#ref": [
            "masterConfig.components.grid.fontSize"
          ]
        },
        "color": {
          "#ref": [
            "masterConfig.components.grid.fontColor"
          ]
        }
      }
    },
    "verifyOtpEditText": {
      "#override": [
        "defaultEditText",
        {
          "icon.height": 25,
          "icon.textVisibility": "visible",
          "icon.visibility": "visible",
          "icon.text": "Resend OTP",
          "icon.textColor": "#634CBB",
          "header.text": "OTP",
          "input.inpType": "Numeric",
          "input.separator": "",
          "input.textMargin": [
            12,
            0,
            12,
            0
          ],
          "focus": true,
          "input.textSize": {
            "#ref": [
              "globalConfig.fontSizeLarge"
            ]
          },
          "margin": [
            10,
            10,
            0,
            12
          ]
        }
      ]
    },
    "defaultEditText": {
      "margin": [
        10,
        10,
        0,
        10
      ],
      "cornerRadius": {
        "#js-expr": [
          "var radius = rc('masterConfig.components.buttons.cornerRadius');\nvar height = rc('defaultPrimaryButton.height');\nvar val = height/2;\nif(val <= radius){\n  radius = val;\n}\n                  radius"
        ]
      },
      "stroke": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.inputFields.type"
                ]
              },
              "underline"
            ]
          },
          "",
          {
            "#js-expr": [
              "var color = rc('masterConfig.components.inputFields.disabledStateColor');\n          \"1,\" + color"
            ]
          }
        ]
      },
      "header": {
        "color": {
          "#ref": [
            "masterConfig.components.inputFields.fieldName.fontColor"
          ]
        },
        "size": {
          "#if": [
            {
              "#js-expr": [
                "window.isAndroid()"
              ]
            },
            {
              "#ref": [
                "masterConfig.components.inputFields.fieldName.fontSize"
              ]
            },
            {
              "#ref": [
                "masterConfig.components.inputFields.fieldName.fontSize"
              ]
            }
          ]
        },
        "padding": {
          "#ref": [
            "masterConfig.components.inputFields.fieldName.padding"
          ]
        }
      },
      "icon": {
        "width": {
          "#js-expr": [
            "rc('globalConfig.iconSize') - 20"
          ]
        },
        "height": {
          "#js-expr": [
            "rc('globalConfig.iconSize') - 20"
          ]
        },
        "textColor": {
          "#ref": [
            "globalConfig.primaryColor"
          ]
        }
      },
      "lineSeparator": {
        "color": {
          "#ref": [
            "masterConfig.components.inputFields.disabledStateColor"
          ]
        },
        "focusedColor": {
          "#ref": [
            "masterConfig.components.inputFields.activeStateColor"
          ]
        }
      },
      "input": {
        "padding": [
          20,
          0,
          20,
          0
        ],
        "height": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            48,
            48
          ]
        },
        "font": {
          "#ref": [
            "globalConfig.fontSemiBold"
          ]
        },
        "width": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            399,
            "match_parent"
          ]
        },
        "textColor": {
          "#ref": [
            "masterConfig.components.inputFields.inputText.fontColor"
          ]
        },
        "textSize": {
          "#ref": [
            "masterConfig.components.inputFields.inputText.fontSize"
          ]
        }
      }
    },
    "addCardSurchargeMessage": {
      "#override": [
        "surchargeMessage",
        {
          "padding": [
            2,
            0,
            4,
            0
          ],
          "text.color": "#A3820E",
          "background": "#ffffff",
          "visibility": "VISIBLE"
        }
      ]
    },
    "surchargeMessage": {
      "#override": [
        "defaultMessage",
        {
          "background": "#F5F6F8",
          "text.size": {
            "#ref": [
              "globalConfig.fontSizeVerySmall"
            ]
          },
          "text.color": {
            "#ref": [
              "globalConfig.textPrimaryColor"
            ]
          },
          "width": "WRAP_CONTENT",
          "text.text": "Convenience fee applicable",
          "visibility": "GONE",
          "padding": [
            0,
            6,
            0,
            6
          ]
        }
      ]
    },
    "nbPrimaryButton": {
      "#override": [
        "defaultPrimaryButton",
        {
          "margin": {
            "#js-expr": [
              "if (window.isDesktopView()) {\n        var cardPadding = rc('masterConfig.components.container.cardPadding');\n        var val = 10 + cardPadding;\n        [25, 0, 0,10]\n      } else {\n        var cardPadding = rc('masterConfig.components.container.cardPadding');\n        if(cardPadding > 0){\n          cardPadding = cardPadding + 11;\n        }\n        [cardPadding, 0, cardPadding, 10]\n       }"
            ]
          }
        }
      ]
    },
    "defaultPrimaryButton": {
      "color": {
        "#ref": [
          "masterConfig.components.buttons.fillColor"
        ]
      },
      "width": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          272,
          "match_parent"
        ]
      },
      "height": 48,
      "cornerRadius": {
        "#js-expr": [
          "var radius = rc('masterConfig.components.buttons.cornerRadius');\nvar height = rc('defaultPrimaryButton.height');\nvar val = height/2;\nif(val <= radius){\n  radius = val;\n}\n                  radius"
        ]
      },
      "margin": {
        "#js-expr": [
          "if (window.isDesktopView()) {\n        [0, 10, 0,0]\n      } else {\n        rc('masterConfig.components.buttons.margin');\n        }"
        ]
      },
      "stroke": {
        "#js-expr": [
          "var strokeColor = rc('masterConfig.components.buttons.strokeColor');\n                  var strokeWidth = rc('masterConfig.components.buttons.strokeWidth');\n                  var isVisible = rc('masterConfig.components.buttons.strokeVisibility');\n                  isVisible == false ? '' : strokeWidth+\",\"+strokeColor"
        ]
      },
      "text": {
        "text": {
          "#ref": [
            "masterConfig.components.buttons.text"
          ]
        },
        "color": {
          "#ref": [
            "masterConfig.components.buttons.fontColor"
          ]
        },
        "size": {
          "#ref": [
            "masterConfig.components.buttons.fontSize"
          ]
        },
        "font": {
          "#ref": [
            "globalConfig.fontSemiBold"
          ]
        }
      },
      "image": {
        "gravity": "LEFT",
        "padding": [
          5,
          0,
          0,
          0
        ]
      }
    },
    "ppAmountBar": {
      "#override": [
        "defaultAmountBar",
        {
          "visibility": {
            "#if": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.visible"
                ]
              },
              "VISIBLE",
              "GONE"
            ]
          }
        }
      ]
    },
    "defaultWebAmountBar": {
      "padding": [
        0,
        20,
        0,
        20
      ],
      "percentWidth": true,
      "width": 94,
      "height": 85,
      "leftSection": {
        "size": 24,
        "font": {
          "#ref": [
            "globalConfig.fontRegular"
          ]
        },
        "color": {
          "#ref": [
            "masterConfig.desktopView.OrderSummary.orderSummaryHeader.fontColor"
          ]
        }
      },
      "lineItems": [
        {
          "leftText": {
            "text": "Subscription type",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              82,
              0,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": "Amount",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.labelText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              151,
              0,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          }
        },
        {
          "leftText": {
            "text": {
              "#js-expr": [
                "var text = \"\";\n                          try {\n                            var orderDetails = JSON.parse(window.__payload.payload.orderDetails);\n                            var description = JSON.parse(orderDetails.description);\n                            text =  description ? description : \"Empty\";\n                          } catch (e) {\n                            text = \"Empty\";\n                          }\n                          text;"
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontSemiBold"
              ]
            },
            "size": {
              "#ref": [
                "globalConfig.fontSizeVeryLarge"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.orderNameText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": 200,
            "padding": [
              81,
              5,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": "<amount>",
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "globalConfig.fontSizeVeryLarge"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.desktopView.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "1,#EBEBEB,r",
            "minWidth": 200,
            "padding": [
              153,
              5,
              0,
              0
            ],
            "gravity": "left",
            "useTextFromHtml": false
          }
        }
      ],
      "translation": {
        "#if": [
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.visible"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.nativeShadow"
            ]
          },
          0
        ]
      },
      "background": {
        "#ref": [
          "masterConfig.desktopView.OrderSummary.backgroundColor"
        ]
      },
      "visibility": {
        "#if": [
          {
            "#ref": [
              "masterConfig.desktopView.OrderSummary.visible"
            ]
          },
          "VISIBLE",
          "GONE"
        ]
      }
    },
    "defaultAmountBar": {
      "padding": [
        0,
        8,
        0,
        4
      ],
      "margin": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.layout"
                ]
              },
              "boxed"
            ]
          },
          [
            20,
            0,
            20,
            0
          ],
          [
            0,
            0,
            0,
            0
          ]
        ]
      },
      "cornerRadius": {
        "#if": [
          {
            "#eq": [
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.layout"
                ]
              },
              "boxed"
            ]
          },
          {
            "#ref": [
              "masterConfig.components.OrderSummary.cornerRadius"
            ]
          },
          0
        ]
      },
      "leftSection": {
        "visibility": {
          "#if": [
            {
              "#js-expr": [
                "window.isDesktopView()"
              ]
            },
            "VISIBLE",
            "VISIBLE"
          ]
        },
        "text": "To Pay",
        "size": 14,
        "margin": [
          28,
          0,
          0,
          4
        ]
      },
      "rightSection": {
        "visibility": "VISIBLE",
        "size": 14,
        "font": {
          "#ref": [
            "globalConfig.fontBold"
          ]
        },
        "margin": [
          0,
          0,
          18,
          4
        ]
      },
      "lineItems": [
        {
          "leftText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "",
                {
                  "#if": [
                    {
                      "#eq": [
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.containerStyle"
                          ]
                        },
                        2
                      ]
                    },
                    "This is your order name",
                    ""
                  ]
                }
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontSize"
                  ]
                },
                {
                  "#ref": [
                    "globalConfig.fontSizeVerySmall"
                  ]
                }
              ]
            },
            "color": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontColor"
                  ]
                },
                {
                  "#ref": [
                    "globalConfig.textSecondaryColor"
                  ]
                }
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              2
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "This is your order name",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.orderNameText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.orderNameText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "rightText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    4
                  ]
                },
                "<amount>",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              0,
              0,
              2
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "padding": [
            0,
            0,
            0,
            0
          ]
        },
        {
          "leftText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                "<amount>",
                {
                  "#js-expr": [
                    "var text = \"\";\nvar type = rc('masterConfig.components.OrderSummary.containerStyle')\n                        try {\n                          var orderDetails = JSON.parse(window.__payload.payload.orderDetails);\n                          var description = JSON.parse(orderDetails.description);\n                          text =  description ? description : \"This is your order name\";\n                        } catch (e) {\n                          text = \"\";\n                        }\n                        type == 3 ? '' : text;"
                  ]
                }
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    2
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.amountText.fontSize"
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontSize"
                  ]
                }
              ]
            },
            "color": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    4
                  ]
                },
                {
                  "#ref": [
                    "masterConfig.components.OrderSummary.orderNameText.fontColor"
                  ]
                },
                {
                  "#if": [
                    {
                      "#eq": [
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.containerStyle"
                          ]
                        },
                        1
                      ]
                    },
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.orderNameText.fontColor"
                      ]
                    },
                    {
                      "#if": [
                        {
                          "#eq": [
                            {
                              "#ref": [
                                "masterConfig.components.OrderSummary.containerStyle"
                              ]
                            },
                            2
                          ]
                        },
                        {
                          "#ref": [
                            "masterConfig.components.OrderSummary.amountText.fontColor"
                          ]
                        },
                        {
                          "#ref": [
                            "globalConfig.textPrimaryColor"
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "centerText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    3
                  ]
                },
                "<amount>",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "rightText": {
            "text": {
              "#if": [
                {
                  "#eq": [
                    {
                      "#ref": [
                        "masterConfig.components.OrderSummary.containerStyle"
                      ]
                    },
                    1
                  ]
                },
                "",
                ""
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontBold"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontSize"
              ]
            },
            "color": {
              "#ref": [
                "masterConfig.components.OrderSummary.amountText.fontColor"
              ]
            },
            "stroke": "",
            "minWidth": "MATCH_PARENT",
            "padding": [
              0,
              {
                "#ref": [
                  "masterConfig.components.OrderSummary.spacingBetween"
                ]
              },
              0,
              0
            ],
            "gravity": "center",
            "useTextFromHtml": false
          },
          "padding": [
            0,
            0,
            0,
            0
          ]
        }
      ],
      "background": {
        "#ref": [
          "masterConfig.components.OrderSummary.containerColor"
        ]
      },
      "visibility": "GONE",
      "dividerColor": "#dddddd",
      "dividerVisibility": "VISIBLE"
    },
    "webBackToolBar": {
      "#override": [
        "webPaymentHeaderToolbar",
        {
          "background": "#ffffff",
          "textSize": {
            "#js-expr": [
              "rc('globalConfig.fontSize')"
            ]
          },
          "leftIcon.visibility": "Visible",
          "visibility": "VISIBLE"
        }
      ]
    },
    "webPaymentHeaderToolbar": {
      "#override": [
        "defaultToolbar",
        {
          "background": {
            "#ref": [
              "screenConfig.bgPrimaryColor"
            ]
          },
          "textSize": {
            "#js-expr": [
              "rc('globalConfig.fontSize') + 4"
            ]
          },
          "leftIcon.visibility": "GONE",
          "text": "Payment Methods",
          "visibility": {
            "#js-expr": [
              "var visibility = \"GONE\"; try{\n  visibility = (window.__OS.toLowerCase() === \"web\") ? (window.__payload.integrationType.toLowerCase() === \"iframe\") ?\n  \"GONE\" : \"GONE\" : \"GONE\";\n} catch(err){\n} visibility;"
            ]
          }
        }
      ]
    },
    "ppToolbar": {
      "#override": [
        "defaultToolbar",
        {
          "visibility": {
            "#js-expr": [
              "var visibility = \"VISIBLE\"; try{\n  visibility = (window.__OS.toLowerCase() === \"web\") ? (window.__payload.integrationType.toLowerCase() === \"iframe\") ?\n  \"VISIBLE\" : \"VISIBLE\" : \"VISIBLE\";\n} catch(err){\n} visibility;"
            ]
          }
        }
      ]
    },
    "defaultToolbar": {
      "divider": {
        "color": "#dddddd",
        "visibility": "visible",
        "height": 1
      },
      "background": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          "#ffffff",
          {
            "#ref": [
              "masterConfig.components.appBar.fillColor"
            ]
          }
        ]
      },
      "text": "Payment Methods",
      "textColor": {
        "#if": [
          {
            "#js-expr": [
              "window.isDesktopView()"
            ]
          },
          {
            "#ref": [
              "globalConfig.textPrimaryColor"
            ]
          },
          {
            "#ref": [
              "masterConfig.components.appBar.fontColor"
            ]
          }
        ]
      },
      "textSize": {
        "#ref": [
          "masterConfig.components.appBar.fontSize"
        ]
      },
      "textGravity": "LEFT",
      "padding": {
        "#ref": [
          "masterConfig.components.appBar.padding"
        ]
      },
      "imageUrl": {
        "#js-expr": [
          "(window.getIcons && JSON.parse(window.getIcons()).toolbarBackArrow) ? JSON.parse(window.getIcons()).toolbarBackArrow : 'toolbar_back_arrow'"
        ]
      },
      "contentMargin": {
        "#js-expr": [
          "var uiPadding = rc('screenConfig.uiCard.horizontalPadding');\n                  var topMargin = rc('flowConfig.drawFromStatusBar') ? window.getStatusBarHeight() : 0;\n                  [21, 0, 0, 0]"
        ]
      },
      "font": {
        "#ref": [
          "globalConfig.fontBold"
        ]
      },
      "translation": {
        "#if": [
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.visible"
            ]
          },
          {
            "#ref": [
              "masterConfig.themes.Shadow.cardShadow.nativeShadow"
            ]
          },
          0
        ]
      },
      "leftIcon": {
        "url": {
          "#js-expr": [
            "(window.getIcons && JSON.parse(window.getIcons()).toolbarBackArrow) ? JSON.parse(window.getIcons()).toolbarBackArrow : 'toolbar_back_arrow'"
          ]
        }
      }
    },
    "unlinkedWalletListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.padding": [
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16,
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16
          ],
          "bottomView.button.height": "48",
          "bottomView.button.margin": {
            "#js-expr": [
              "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0];\n      if(window.isDesktopView()){\n        val = val + 12;\n      }\n      [val, 0, 5, 0]"
            ]
          },
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              275,
              "match_parent"
            ]
          },
          "bottomView.editText.visibility": "GONE"
        }
      ]
    },
    "upiAppListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.rightImage.visibility": "visible",
          "topView.rightImage.usePackageIcon": false,
          "topView.isClickable": true,
          "bottomView.bottomDefaultExpand": false,
          "bottomView.editText.visibility": "gone",
          "bottomView.fifthLine.visibility": "gone",
          "divider.visibility": "gone"
        }
      ]
    },
    "nbScreenOtherBanksListItem": {
      "#override": [
        "defaultListItem",
        {
          "bottomView.editText.visibility": "gone",
          "bottomView.button.width": "match_parent"
        }
      ]
    },
    "ppSavedCardListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                18,
                10,
                26,
                10
              ],
              [
                {
                  "#ref": [
                    "masterConfig.components.container.cardPadding"
                  ]
                },
                16,
                {
                  "#ref": [
                    "masterConfig.components.container.cardPadding"
                  ]
                },
                16
              ]
            ]
          },
          "topView.secondLine.visibility": "visible",
          "topView.firstLine.textOne.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                8,
                0,
                0,
                0
              ],
              [
                7,
                0,
                0,
                0
              ]
            ]
          },
          "topView.secondLine.text.margin": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                8,
                {
                  "#ref": [
                    "masterConfig.components.listItems.spacingBetween"
                  ]
                },
                0,
                0
              ],
              [
                7,
                {
                  "#ref": [
                    "masterConfig.components.listItems.spacingBetween"
                  ]
                },
                0,
                0
              ]
            ]
          },
          "topView.secondLine.text.visibility": "visible",
          "bottomView.editTextWeight": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              0.15,
              0.25
            ]
          },
          "bottomView.buttonWeight": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              0.85,
              0.75
            ]
          },
          "bottomView.button.margin": {
            "#js-expr": [
              "if (window.isDesktopView()) {\n                [20, 0, 0, 0]\n              } else {\n                [16, 0, 0, 0]\n              }"
            ]
          },
          "bottomView.editText.hint.text": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              "C V V",
              "●●●"
            ]
          },
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              250,
              "match_parent"
            ]
          },
          "bottomView.button.height": "48",
          "bottomView.editText.input.height": "48",
          "bottomView.editText.input.gravity": "CENTER_HORIZONTAL",
          "bottomView.padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\nvar cardPadding = rc('masterConfig.components.container.cardPadding');\n      var val = padding[0] + cardPadding + 12;\n      [val, 0, 0, 16]"
                ]
              },
              {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0] + 13;\n      [val, 0, 4, 16]"
                ]
              }
            ]
          },
          "bottomView.editText.input.padding": [
            10,
            4,
            10,
            4
          ],
          "bottomView.editText.focus": true,
          "bottomView.editText.stroke": {
            "#if": [
              {
                "#eq": [
                  {
                    "#ref": [
                      "masterConfig.components.inputFields.type"
                    ]
                  },
                  "underline"
                ]
              },
              "",
              {
                "#js-expr": [
                  "var color = rc(\"masterConfig.components.inputFields.activeStateColor\"); \"1,\"+ color"
                ]
              }
            ]
          },
          "bottomView.editText.input.letterSpacing": 1,
          "bottomView.editText.input.textSize": 12,
          "bottomView.editText.input.inpType": "NumericPassword",
          "bottomView.editText.icon.visibility": "GONE",
          "bottomView.fifthLine.visibility": "gone"
        }
      ]
    },
    "savedVPAListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.firstLine.textTwo.visibility": "gone",
          "topView.secondLine.visibility": "visible",
          "topView.secondLine.text.visibility": "visible",
          "bottomView.editText.visibility": "gone",
          "bottomView.button.width": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              275,
              "match_parent"
            ]
          }
        }
      ]
    },
    "linkedWalletListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.padding": [
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16,
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16
          ],
          "bottomView.button.margin": {
            "#js-expr": [
              "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0];\n      if(window.isDesktopView()){\n        val = val + 12;\n      }\n      [val, 0, 5, 0]"
            ]
          },
          "bottomView.button.height": "48"
        }
      ]
    },
    "retrySuggestionListItem": {
      "#override": [
        "defaultListItem",
        {
          "background": "#fafbfb",
          "topView.firstLine.visibility": "visible",
          "topView.firstLine.textOne.visibility": "visible",
          "topView.firstLine.textTwo.visibility": "gone",
          "topView.firstLine.imageOne.visibility": "gone",
          "topView.thirdLine.text.text": "Experience One Click payment with Pay Later",
          "topView.thirdLine.text.margin": [
            0,
            10,
            0,
            10
          ],
          "topView.rightSectionVisibility": "GONE",
          "topView.isClickable": false,
          "topView.secondLine.text.color": "#909191",
          "topView.secondLine.text.visibility": "VISIBLE",
          "bottomView.visibility": "VISIBLE",
          "bottomView.margin": [
            0,
            0,
            0,
            0
          ],
          "bottomView.button.visibility": "VISIBLE",
          "bottomView.button.height": 48,
          "bottomView.button.margin": [
            16,
            0,
            16,
            0
          ],
          "bottomView.button.cornerRadius": 4,
          "bottomView.button.text.font": {
            "#ref": [
              "globalConfig.fontBold"
            ]
          },
          "bottomView.button.text.padding": [
            70,
            15,
            70,
            15
          ],
          "divider.visibility": "GONE",
          "bottomView.divider": "GONE"
        }
      ]
    },
    "ppSavedVPAListItem": {
      "#override": [
        "defaultListItem",
        {
          "topView.padding": [
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16,
            {
              "#js-expr": [
                "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
              ]
            },
            16
          ],
          "bottomView.button.height": "48",
          "topView.secondLine.text.visibility": "VISIBLE",
          "topView.secondLine.visibility": "VISIBLE",
          "bottomView.editText.visibility": "GONE",
          "bottomView.button.margin": {
            "#js-expr": [
              "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0];\n      if(window.isDesktopView()){\n        val = val + 12;\n      }\n      [val, 0, 5, 0]"
            ]
          }
        }
      ]
    },
    "defaultListItem": {
      "topView": {
        "firstLine": {
          "textOne": {
            "color": {
              "#ref": [
                "masterConfig.components.listItems.mainText.fontColor"
              ]
            },
            "font": {
              "#ref": [
                "globalConfig.fontRegular"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.listItems.mainText.fontSize"
              ]
            },
            "margin": [
              7,
              0,
              0,
              0
            ],
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          },
          "imageOne": {
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          }
        },
        "secondLine": {
          "text": {
            "color": {
              "#ref": [
                "masterConfig.components.listItems.subText.fontColor"
              ]
            },
            "size": {
              "#ref": [
                "masterConfig.components.listItems.subText.fontSize"
              ]
            },
            "margin": [
              7,
              {
                "#ref": [
                  "masterConfig.components.listItems.spacingBetween"
                ]
              },
              0,
              0
            ],
            "padding": {
              "#ref": [
                "masterConfig.components.listItems.paddings"
              ]
            }
          }
        },
        "leftImage": {
          "size": {
            "#ref": [
              "masterConfig.components.listItems.primaryIconSize"
            ]
          },
          "margin": [
            0,
            0,
            0,
            0
          ],
          "padding": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              [
                5,
                5,
                5,
                5
              ],
              [
                0,
                0,
                0,
                0
              ]
            ]
          }
        },
        "rightImage": {
          "size": {
            "#ref": [
              "masterConfig.components.listItems.secondaryIconSize"
            ]
          }
        },
        "selectionLabel": {
          "size": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              {
                "#ref": [
                  "globalConfig.fontSize"
                ]
              },
              {
                "#ref": [
                  "masterConfig.components.links.fontSize"
                ]
              }
            ]
          },
          "color": {
            "#ref": [
              "masterConfig.components.links.fontColor"
            ]
          },
          "font": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          },
          "background": {
            "#if": [
              {
                "#js-expr": [
                  "window.isDesktopView()"
                ]
              },
              "#ffffff",
              {
                "#ref": [
                  "masterConfig.themes.Colors.defaultTileColor"
                ]
              }
            ]
          },
          "cornerRadius": 2,
          "stroke": ""
        },
        "height": 56,
        "topView.padding": [
          {
            "#js-expr": [
              "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
            ]
          },
          16,
          {
            "#js-expr": [
              "var padding = rc('masterConfig.components.container.cardPadding');\n                  window.isDesktopView() ? 20 : padding"
            ]
          },
          16
        ]
      },
      "bottomView": {
        "editText": {
          "#override": [
            "defaultEditText",
            {
              "input.height": 48,
              "focus": true,
              "visibility": "GONE"
            }
          ]
        },
        "button": {
          "#override": [
            "defaultPrimaryButton",
            {
              "margin": {
                "#js-expr": [
                  "var padding = rc('masterConfig.components.listItems.paddings');\n      var val = padding[0] + 18;\n      [val, 0, 5, 0]"
                ]
              },
              "height": 48,
              "width": "match_parent"
            }
          ]
        },
        "margin": {
          "#js-expr": [
            "var imageSize = rc('defaultListItem.topView.leftImage.size');\n      var pX = rc('screenConfig.uiCard.horizontalPadding') - 6;\n      if (window.isDesktopView()) pX = 3;\n      var space = pX + imageSize;\n      [space, 0, pX, 0]"
          ]
        },
        "padding": {
          "#js-expr": [
            "var pY = rc('screenConfig.uiCard.horizontalPadding');\n      [12, 0, 0, 16]"
          ]
        }
      },
      "divider": {
        "visibility": "gone"
      }
    },
    "upiScreenEditText": {
      "#override": [
        "defaultEditText",
        {
          "icon.textColor": "#444444"
        }
      ]
    },
    "cardNumberConfig": {
      "#override": [
        "defaultEditText",
        {
          "input.separator": {
            "#js-expr": [
              "' '"
            ]
          },
          "input.separatorRepeat": "4",
          "input.inpType": "numeric",
          "input.pattern": "^([0-9]| )+$,24",
          "icon.width": 40,
          "icon.height": 40
        }
      ]
    },
    "cvvConfig": {
      "#override": [
        "defaultEditText",
        {
          "input.inpType": "NumericPassword",
          "input.pattern": "^[0-9]+$,3",
          "visibility": "VISIBLE",
          "icon.visibility": "visible",
          "icon.width": {
            "#ref": [
              "globalConfig.checkboxSize"
            ]
          },
          "icon.height": {
            "#ref": [
              "globalConfig.checkboxSize"
            ]
          }
        }
      ]
    },
    "expiryDateConfig": {
      "#override": [
        "defaultEditText",
        {
          "hint.text": "MM / YY",
          "input.separator": "/",
          "input.separatorRepeat": "2",
          "input.inpType": "Numeric",
          "input.pattern": "^([0-9]|\\/)+$,5",
          "visibility": "VISIBLE",
          "icon.textVisibility": "gone",
          "icon.visibility": "visible",
          "input.width": "wrap_content",
          "error.textFont": {
            "#ref": [
              "globalConfig.fontSemiBold"
            ]
          }
        }
      ]
    }
  }
};
      return JSON.stringify(configuration);
      }