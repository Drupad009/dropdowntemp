window.hyperpay_icons.js_version = 2.0.0;
if (window.JOS && typeof window.JOS.registerVersion == "function"){
  window.JOS.registerVersion(window.JOS.self)("icons")(window.hyperpay_icons_version)();
}
window.getIcons = function () {
  return JSON.stringify({"upi":"https://i.ibb.co/7pztNDs/f161be552179.png?color=803939&iconName=jp_upi_01","card":"https://i.ibb.co/7Kxtp7c/3b726b3661c4.png?color=ca3838&iconName=jp_card_02"})
};